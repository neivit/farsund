@extends('layouts.app')
@section('css')
<style>
    .input-date {
        position: relative;
        display: block;
        margin: 0;
        padding: 0;
        padding-left: 15px;
        padding-top: 9px;
        padding-bottom: 10px;
        width: 100%;
        font-family: 'Poppins', sans-serif;
        font-size: 13px;
        line-height: 1.2;
        letter-spacing: 1px;
        font-weight: 300;
        text-align: left;
        color: #fff;
        border: solid 1px rgba(255, 255, 255, .1);
        outline: none;
        background-color: #7c7c7c;
        transition: border-color .2s ease-out;
        background-color: transparent;
        border: solid 1px rgba(255, 255, 255, .1);
    }

    ::-webkit-calendar-picker-indicator {
        filter: invert(1);
    }

    ::placeholder {
        color: #fff;
        font-size: 13px;
        text-transform: uppercase;
    }

</style>
@endsection
@section('content')
<div class="section hero-full-height over-hide">

    <div id="poster_background"></div>
    <div id="video-wrap" class="parallax-top">
        <video id="video_background" preload="auto" autoplay loop="loop" muted="muted" poster="img/trans.png">
            <source src="{{asset('img/video/header.mp4')}}" type="video/mp4">
        </video>
    </div>
    <div class="dark-over-video"></div>

    <div class="hero-center-section ver-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 parallax-fade-top">
                    <form action="{{route('rooms')}}" id="booking">
                        <div class="hero-center-section">
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-10 col-sm-8 parallax-fade-top">
                                    </div>
                                    <div class="col-12 mt-3 parallax-fade-top">
                                        <div class="booking-hero-wrap">
                                            <div class="row justify-content-center">
                                                <div class="col-5 no-mob">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="form-item">
                                                                <span class="fontawesome-calendar"></span>
                                                                <input type="date" name="check_in" class="input-date"
                                                                    id="chechin" placeholder="chech-in"
                                                                    min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")));?>" />
                                                                <span class="date-text date-depart"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-item">
                                                                <span class="fontawesome-calendar"></span>
                                                                <input type="date" name="check_out" class="input-date"
                                                                    id="checkout" placeholder="check-out"
                                                                    min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")));?>" />
                                                                <span class="date-text date-return"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-5 no-mob">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <select name="adult" class="wide">
                                                                <option>adultos</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <select name="children" class="wide">
                                                                <option>Niños</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6  col-sm-4 col-lg-2">
                                                    <button class="booking-button" class="booking-button"
                                                        style="cursor: pointer;border: 0;">{{trans('messages.buscar')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section padding-top-bottom over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-6 align-self-center">
                <div class="row justify-content-center">
                    <div class="col-10">
                        <div class="subtitle text-center mb-4">hotel Farsund</div>
                        <h2 class="text-center">{{trans('messages.mejor_hotel')}}</h2>
                        <p class="text-center mt-5">{{trans('messages.farsund_descripcion')}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-4 mt-md-0">
                <div class="img-wrap">
                    <img src="img/about.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section background-grey over-hide">
    <div class="container-fluid px-0">
        <div class="row mx-0">
            <div class="col-xl-6 px-0">
                <div class="img-wrap" id="rev-1">
                    <img src="img/about2.jpg" alt="">
                </div>
            </div>
            <div class="col-xl-6 px-0 mt-4 mt-xl-0 align-self-center">
                <div class="row justify-content-center">
                    <div class="col-10 col-xl-8 text-center">
                        <h3 class="text-center">{{trans('messages.sientate_casa')}}</h3>
                        <p class="text-center mt-4">Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                            accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                            veritatis et.</p>
                        <a class="mt-5 btn btn-primary"
                            href="{{route('rooms')}}">{{trans('messages.consultar_disponibilidad')}}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mx-0">
            <div class="col-xl-6 px-0 mt-4 mt-xl-0 pb-5 pb-xl-0 align-self-center">
                <div class="row justify-content-center">
                    <div class="col-10 col-xl-8 text-center">
                        <h3 class="text-center">{{trans('messages.suite_vista_mar')}}</h3>
                        <p class="text-center mt-4">Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                            accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                            veritatis et.</p>
                        <a class="mt-5 btn btn-primary"
                            href="{{route('rooms')}}">{{trans('messages.consultar_disponibilidad')}}</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 px-0 order-first order-xl-last mt-5 mt-xl-0">
                <div class="img-wrap" id="rev-2">
                    <img src="img/about3.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section background-dark over-hide">
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <a href="{{route('services')}}">
                    <div class="img-wrap services-wrap">
                        <img src="img/spa-salon.jpg" alt="">
                        <div class="services-text-over">Spa</div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-3 pt-4 pt-sm-0">
                <a href="{{route('services')}}">
                    <div class="img-wrap services-wrap">
                        <img src="img/restaurant.jpg" alt="">
                        <div class="services-text-over">{{trans('messages.restaurantes')}}</div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-3 pt-4 pt-lg-0">
                <a href="{{route('services')}}">
                    <div class="img-wrap services-wrap">
                        <img src="img/piscina.jpg" alt="">
                        <div class="services-text-over">{{trans('messages.piscina')}}</div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-3 pt-4 pt-lg-0">
                <a href="{{route('services')}}">
                    <div class="img-wrap services-wrap">
                        <img src="img/recreacion.jpg" alt="">
                        <div class="services-text-over">{{trans('messages.actividades')}}</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="section padding-top-bottom over-hide">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 align-self-center">
                <div class="subtitle with-line text-center mb-4">Hotel Fadsund</div>
                <h3 class="text-center padding-bottom-small">{{trans('messages.lujos_pretenciones')}}</h3>
            </div>
            <div class="section clearfix"></div>
            <div class="col-sm-6 col-lg-4">
                <div class="services-box text-center">
                    <img src="img/1.svg" alt="">
                    <h5 class="mt-2">{{trans('messages.estacionamiento')}}</h5>
                    <p class="mt-3">Sed ut perspiciatis unde omnis iste natus error sit, totam rem aperiam, eaque ipsa
                        quae ab illo inventore veritatis et.</p>
                    <a class="mt-1 btn btn-primary" href="{{route('services')}}">{{trans('messages.leer_mas')}}</a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 mt-5 mt-sm-0">
                <div class="services-box text-center">
                    <img src="img/2.png" alt="">
                    <h5 class="mt-2">{{trans('messages.playa')}}</h5>
                    <p class="mt-3">Sed ut perspiciatis unde omnis iste natus error sit, totam rem aperiam, eaque ipsa
                        quae ab illo inventore veritatis et.</p>
                    <a class="mt-1 btn btn-primary" href="{{route('services')}}">{{trans('messages.leer_mas')}}</a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 mt-5 mt-lg-0">
                <div class="services-box text-center">
                    <img src="img/3.png" alt="">
                    <h5 class="mt-2">{{trans('messages.wifi')}}</h5>
                    <p class="mt-3">Sed ut perspiciatis unde omnis iste natus error sit, totam rem aperiam, eaque ipsa
                        quae ab illo inventore veritatis et.</p>
                    <a class="mt-1 btn btn-primary" href="{{route('services')}}">{{trans('messages.leer_mas')}}</a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 mt-5">
                <div class="services-box text-center">
                    <img src="img/4.svg" alt="">
                    <h5 class="mt-2">{{trans('messages.restaurantes')}}</h5>
                    <p class="mt-3">Sed ut perspiciatis unde omnis iste natus error sit, totam rem aperiam, eaque ipsa
                        quae ab illo inventore veritatis et.</p>
                    <a class="mt-1 btn btn-primary" href="{{route('services')}}">{{trans('messages.leer_mas')}}</a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 mt-5">
                <div class="services-box text-center">
                    <img src="img/5.svg" alt="">
                    <h5 class="mt-2">{{trans('messages.bar')}}</h5>
                    <p class="mt-3">Sed ut perspiciatis unde omnis iste natus error sit, totam rem aperiam, eaque ipsa
                        quae ab illo inventore veritatis et.</p>
                    <a class="mt-1 btn btn-primary" href="{{route('services')}}">{{trans('messages.leer_mas')}}</a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 mt-5">
                <div class="services-box text-center">
                    <img src="img/6.png" alt="">
                    <h5 class="mt-2">{{trans('messages.comida_incluida')}}</h5>
                    <p class="mt-3">Sed ut perspiciatis unde omnis iste natus error sit, totam rem aperiam, eaque ipsa
                        quae ab illo inventore veritatis et.</p>
                    <a class="mt-1 btn btn-primary" href="{{route('services')}}">{{trans('messages.leer_mas')}}</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section padding-top-bottom-big over-hide">
    <div class="parallax" style="background-image: url('img/section-home.jpg')"></div>
    <div class="section z-bigger">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row justify-content-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="section padding-top-bottom background-grey over-hide">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 align-self-center">
                <div class="subtitle with-line text-center mb-4">{{trans('messages.restaurantes')}}</div>
                <h3 class="text-center padding-bottom-small">{{trans('messages.bar_copas')}}</h3>
            </div>
            <div class="section clearfix"></div>
        </div>
        <div class="row background-white p-0 m-0">
            <div class="col-xl-6 p-0">
                <div class="img-wrap" id="rev-3">
                    <img src="img/rest.jpg" alt="">
                </div>
            </div>
            <div class="col-xl-6 p-0 align-self-center">
                <div class="row justify-content-center">
                    <div class="col-9 pt-4 pt-xl-0 pb-5 pb-xl-0 text-center">
                        <h5 class="" style="text-transform: none">{{trans('messages.restaurant_playa')}}</h5>
                        <p class="mt-3">Sed ut perspiciatis unde omnis, totam rem aperiam, eaque ipsa quae ab illo
                            inventore veritatis et.</p>
                        <a class="mt-1 btn btn-primary" href="#">{{trans('messages.explorar')}}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row background-white p-0 m-0">
            <div class="col-xl-6 p-0 align-self-center">
                <div class="row justify-content-center">
                    <div class="col-9 pt-4 pt-xl-0 pb-5 pb-xl-0 text-center">
                        <h5 class="" style="text-transform: none">{{trans('messages.restaurant_piscina')}}</h5>
                        <p class="mt-3">Sed ut perspiciatis unde omnis, totam rem aperiam, eaque ipsa quae ab illo
                            inventore veritatis et.</p>
                        <a class="mt-1 btn btn-primary" href="#">{{trans('messages.explorar')}}</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 order-first order-xl-last p-0">
                <div class="img-wrap" id="rev-4">
                    <img src="img/rest2.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    window.addEventListener('load', function () {

        document.getElementById('chechin').type = 'chech-in date';

        document.getElementById('chechin').addEventListener('blur', function () {

            document.getElementById('chechin').type = 'chech-in date';

        });

        document.getElementById('chechin').addEventListener('focus', function () {

            document.getElementById('chechin').type = 'date';

        });

    });



    window.addEventListener('load', function () {

        document.getElementById('checkout').type = 'check-out date';

        document.getElementById('checkout').addEventListener('blur', function () {

            document.getElementById('checkout').type = 'check-out date';

        });

        document.getElementById('checkout').addEventListener('focus', function () {

            document.getElementById('checkout').type = 'date';

        });

    });

</script>
@endsection
