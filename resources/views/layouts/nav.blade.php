<svg class="hidden">
		<svg id="icon-nav" viewBox="0 0 152 63">
			<title>navarrow</title>
			<path d="M115.737 29L92.77 6.283c-.932-.92-1.21-2.84-.617-4.281.594-1.443 1.837-1.862 2.765-.953l28.429 28.116c.574.57.925 1.557.925 2.619 0 1.06-.351 2.046-.925 2.616l-28.43 28.114c-.336.327-.707.486-1.074.486-.659 0-1.307-.509-1.69-1.437-.593-1.442-.315-3.362.617-4.284L115.299 35H3.442C2.032 35 .89 33.656.89 32c0-1.658 1.143-3 2.552-3H115.737z"/>
		</svg>
	</svg>

	
	<!-- Nav and Logo
	================================================== -->

	<nav id="menu-wrap" class="menu-back cbp-af-header">
		<div class="menu-top background-black">
			<div class="container">
				<div class="row">
					<div class="col-6 px-0 px-md-3 pl-1 py-3">
					</div>
					<div class="col-6 px-0 px-md-3 py-3 text-right">
					@if(Auth::check())
						@if(Auth::user()->admin == 1 )
						  <a href="{{route('profile.index')}}" class="social-top">Dashboard</a>
						@endif
						<strong class="social-top">{{ ucwords(trans(auth()->user()->firstname)) }}</strong>
						<a href="{{route('logout.user')}}" class="logout">{{trans('messages.salir')}}</a>
					@else
						<a href="{{route('login')}}" class="social-top">Iniciar sesión</a>
						<a href="{{route('create.user')}}" class="social-top">Registro</a>
					@endif
						<div class="lang-wrap">
							{{trans('messages.idioma')}}
						 <ul>
							<!--Comprobamos si el status esta a true y existe más de un lenguaje-->
							@if(config('locale.status') && count(config('locale.languages')) > 1)	
								@foreach(array_keys(config('locale.languages')) as $lang)
								@if($lang != App::getLocale())  
									<li><a href="{{route('lang.swap', $lang)}}">{{$lang}}</a></li>
								@endif
								@endforeach
							@endif
						 </ul>
						</div>
					</div>
				</div>	
			</div>		
		</div>
		<div class="menu">
			<a href="{{route('home')}}" >
				<div class="logo">
					<img src="{{asset('img/logo/logo.png')}}">
				</div>
			</a>
			<ul>
				<li><a class="{{ Route::currentRouteName() == 'home' ? 'curent-page' : '' }}" href="{{route('home')}}" >{{ trans('messages.inicio') }}</a></li>
                <li><a class="{{ Route::currentRouteName() == 'about' ? 'curent-page' : '' }}" href="{{route('about')}}">{{trans('messages.nosotros')}}</a></li>
				<li><a class="{{ Route::currentRouteName() == 'rooms' ? 'curent-page' : '' }}" href="{{route('rooms')}}" >{{trans('messages.habitaciones')}}</a></li>
				<li><a class="{{ Route::currentRouteName() == 'services' ? 'curent-page' : '' }}" href="{{route('services')}}" >{{trans('messages.servicios')}}</a></li>
				<li><a class="{{ Route::currentRouteName() == 'places_interest' ? 'curent-page' : '' }}" href="{{route('places_interest')}}" >{{trans('messages.lugar_interes')}}</a></li>
				<li><a class="{{ Route::currentRouteName() == 'contact' ? 'curent-page' : '' }}" href="{{route('contact')}}">{{trans('messages.contacto')}}</a></li>
			@if(Auth::check())
			@else
				<li class="d-block d-sm-block d-md-none"><a href="{{route('login')}}">Iniciar sesión</a></li>
				<li class="d-block d-sm-block d-md-none"><a href="{{route('create.user')}}">Registro</a></li>
			@endif
			</ul>
		</div>
	</nav>