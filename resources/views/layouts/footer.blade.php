<div class="section py-4 background-dark over-hide footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-5 text-center text-md-left mb-1 mb-md-0 desktop-copyright">
					<p>Copyright © 2021 Tódolos Dereitos Reservados Desarrollado por <a href="http://www.mais.gal/" style="color:#00ffff"> Máis Deseño</a></p>
				</div>
				<div class="col-md-3 text-center text-md-right">
					<img src="{{asset('img/icon-footer.png')}}" width="300px" style="position:relative;bottom:8px;">
				</div>
				<div class="col-md-4 text-center text-md-right">
				<p>
					<a href="{{route('terms.conditions')}}" style="color:#ccc;">Terminos y Condiciones</a> 
					<a href="{{route('policy.privacy')}}" style="color:#ccc;">Privacidad y Cookies</a>
					</p>
				</div>
				<div class="col-md-5 text-center text-md-left mb-1 mb-md-0 mobile-copyright">
					<p>Copyright © 2021 Tódolos Dereitos Reservados Desarrollado por <a href="http://www.mais.gal/" style="color:#00ffff"> Máis Deseño</a></p>
				</div>
			</div>	
		</div>		
	</div>
	
	
	<div class="scroll-to-top"></div>
	
	
	<!-- JAVASCRIPT
    ================================================== -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
	<script src="{{ asset('js/popper.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/plugins.js') }}"></script>
    <!-- booking page -->
	<script src="{{asset('js/flip-slider.js')}}"></script>  
	<script src="{{asset('js/reveal-home.js')}}"></script>  
    <!-- booking page -->
	<script src="{{ asset('js/custom.js') }}"></script>
<!-- End Document
================================================== -->

