<!DOCTYPE html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<title>Farsund</title>
	<meta name="description"  content="Farsund Hotel" />
	<meta name="author" content="Máis Deseño ">
	<meta name="keywords"  content="hotel, sanxenxo, hotel sanxexo, habitaciones sanxexo" />
	<meta property="og:title" content="Farsund Hote - Sanxenxo" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="http://www.hotelfarsund.es/" />
	<meta property="og:site_name" content="Farsund Hotel" />
	<meta property="og:description" content="Hotel Farsund" />
	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="theme-color" content="#212121"/>
    <meta name="msapplication-navbutton-color" content="#212121"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="#212121"/>
	@include('layouts.head');
    @yield('css')
	
</head>
<body>
	@if(Request::is('/'))
	<div id="overbox3">
		<div id="infobox3">
		  <div class="wrapper">
			<div class="text-polity">Farsund Hotel, le informa que este sitio utiliza cookies de terceros con fines analíticos y para la elaboración de perfiles basados en hábitos de navegación que permiten a dichos terceros mostrarle publicidad relacionada con sus preferencias. Puedes aceptar todas las cookies pulsando el botón “Aceptar” o rechazar su uso seleccionado la opción “Rechazarlas todas”. Igualmente podrás configurarlas seleccionando la opción “Configura tus cookies”. Para consultar nuestra política de cookies</div>
			<div class="button-cookies" style="margin-top:50px;">
				<button type="button" class="cookie-setting-link" style="margin-right:10px" data-toggle="modal" data-target="#exampleModal">Configura tus cookies</button>
				<button class="reject-all" style="margin-right:10px" onclick="aceptar_cookies();">Rechazarlas todas</button>
				<button class="accept" onclick="aceptar_cookies();">Aceptar todas las cookies</button>
			</div>
	      </div>
  		</div>
	</div>


	<!-- Configure -->
	<div style="z-index: 999999;" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="background-color: #141414;">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><img class="logo-cookies"src="{{asset('img/logo/logo.png')}}"></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span style="color:#fff" aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
		Tu privacidad Farsund Hotel, le informa que este sitio utiliza cookies de terceros con fines analíticos y para la elaboración de perfiles basados en hábitos de navegación que permiten a dichos terceros mostrarle publicidad relacionada con sus preferencias. Puedes aceptar todas las cookies pulsando el botón “Aceptar” o rechazar su uso seleccionado la opción “Rechazarlas todas”. Igualmente podrás configurarlas seleccionando la opción “Configura tus cookies”. Para consultar nuestra
		<a href="{{route('policy.privacy')}}">Política de Cookies</a>
		</div>
		<div class="modal-body">
		<button class="accept" onclick="aceptar_cookies();">Aceptar todas las cookies</button>
		</div>
		<div class="modal-body">
		  <p class="text-gets">Gestionar las preferencias de consentimiento</p>
		</div>
		<div class="modal-body">
				<div id="accordion">
		<div class="card">
			<div class="card-header" id="headingOne">
			<h5 class="mb-0" >
				<button style="color:#fff" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				Cookies estrictamente necesarias
				</button>
			</h5>
			</div>

			<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
			<div class="card-body">
			son aquellas que permiten al usuario la navegación a través de una página web, plataforma o aplicación y la utilización de las diferentes opciones o servicios que en ella existan, incluyendo aquellas que el editor utiliza para permitir la gestión y operativa de la página web, plataforma o aplicación y habilitar sus funciones y servicios, como, por ejemplo, controlar el tráfico y la comunicación de datos, identificar la sesión, acceder a partes de acceso restringido, recordar los elementos que integran un pedido, entre otros. Estas cookies no guardan ninguna información personal identificable.
			</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header" id="headingTwo">
				<h5 class="mb-0">
					<button style="color:#fff" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
					Cookies de rendimiento y análisis 
					</button>
				</h5>
			</div>
			<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
				<div class="card-body">
					Son aquellas que permiten cuantificar el número de usuarios y así realizar la medición y análisis estadístico de la utilización que hacen los usuarios del servicio ofertado.
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header" id="headingThree">
				<h5 class="mb-0">
					<button style="color:#fff" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
						Cookies de publicidad comportamental
					</button>
				</h5>
			</div>
			<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
			<div class="card-body">
				Son aquéllas que permiten analizar sus hábitos de navegación en Internet y mostrarle, en base a ellos, publicidad relacionada con su perfil de navegación. Si no permite estas cookies, tendrá menos publicidad dirigida.
			</div>
			</div>
		</div>
		</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="accept" data-dismiss="modal" onclick="aceptar_cookies();">Confirmar mis preferencias</button>
		</div>
		</div>
	</div>
	</div>
	@endif	
	
    @include('layouts.nav')
    
    <!-- Layout ===== -->
    @yield('content')

    @include('layouts.footer')
    @yield('js')
	
	@if(Request::is('/'))
	<script>

$(document).ready(function() {  
		$("#overbox3").animate( { "opacity": "show", top:"1"} , 700 );
	});
		function GetCookie(name) {
    var arg=name+"=";
    var alen=arg.length;
    var clen=document.cookie.length;
    var i=0;

    while (i<clen) {
        var j=i+alen;

        if (document.cookie.substring(i,j)==arg)
            return "1";
        i=document.cookie.indexOf(" ",i)+1;
        if (i==0)
            break;
    }

    return null;
}

function aceptar_cookies(){
    var expire=new Date();
    expire=new Date(expire.getTime()+7776000000);
    document.cookie="cookies_surestao=aceptada; expires="+expire;

    var visit=GetCookie("cookies_surestao");

    if (visit==1){
        popbox3();
		
    }
	
}

$(function() {
    var visit=GetCookie("cookies_surestao");
    if (visit==1){ popbox3(); }
});

function popbox3() {
	$('.modal').modal('hide');
    $('#overbox3').toggle();
}
	
	</script>
	@endif	
</body>
</html>