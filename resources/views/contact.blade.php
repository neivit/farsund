@extends('layouts.app')
 @section('css')
  <style>
	  p{
		  color:black
	  }
	 p span{
		color:black;
		font-weight:800
	  }
	  label{
		color:black;
		font-weight:800
	  }
  </style>
 @endsection
 @section('content')
	<div class="section padding-top-bottom-smaller background-dark-2 over-hide">		
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">	
					
				</div>
			</div>
		</div>	
	</div>
	
	<div class="section padding-top z-bigger">
		<div class="container">
		<form method="post" action="{{route('send.contact')}}">
		@csrf
			<div class="row justify-content-center padding-bottom-smaller">
				<div class="col-md-8">
					<div class="subtitle with-line text-center mb-4">Farsund</div>
					@if (\Session::has('success'))
						<div class="alert alert-success">
							{{\Session::get('success')}}
						</div>
					@endif
					<h3 class="text-center padding-bottom-small">{{trans('messages.contacto')}}</h3>
				</div>
				<div class="section clearfix"></div>
				<div class="col-md-4 ajax-form">
					<input name="name" type="text" class="{{ $errors->has('name') ? 'is-invalid' : '' }}" placeholder="{{trans('messages.tu_nombre')}}: *" autocomplete="off"/>
				</div>
				<div class="col-md-4 mt-4 mt-md-0 ajax-form">
					<input name="email" type="text"  class="{{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="{{trans('messages.tu_email')}}: *" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" autocomplete="off"/>
				</div>
				<div class="section clearfix"></div>
				<div class="col-md-8 mt-4 ajax-form">
					<textarea name="message" class="{{ $errors->has('message') ? 'is-invalid' : '' }}" placeholder="{{trans('messages.tu_mensaje')}}"></textarea>
				</div>
				<div class="section clearfix"></div>
				<div class="col-md-8 mt-3 ajax-checkbox">
					<p>
						De conformidad con lo dispuesto en la normativa vigente en protección de datos personales LO 3/2018 (LOPDGDD) y Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo del 27 de abril de 2016, solicitamos su consentimiento y le informamos de:
					</p>
					<br>
					<p><span>Responsable</span> del tratamiento de sus datos es: Hotel Farsund</p>
					<p><span>Finalidad</span> con la que se tratar los datos facilitados es: Gestión y resolución las cuestiones planteadas a través de este formulario de contacto por los usuarios de la página Web.</p>
					<p><span>Base legitimadora:</span> Consentimiento</p>
					<p><span>Destinatarios:</span> No comunicamos sus datos a ningún tercero, salvo el gestor de correo electrónico y alojamiento Web.</p>
					<p><span>Derechos:</span> Le informamos que puede ejercer los derechos de acceso, rectificación, supresión, limitación y oposición al tratamiento enviando un email a <span>info@hotelfarsund.es</span></p>
					<p><span>Más información:</span> Puede consultar información adicional sobre protección de datos en la Política de Privacidad</p>
					<div class="form-group">
    					<div class="form-check">
							<input class="form-check-input" type="checkbox" name="check1" id="gridCheck">
							<label class="form-check-label" for="gridCheck">
							Doy mi consentimiento para recibir ofertas y correos comerciales.
							</label>
							<br>
							<input class="form-check-input" type="checkbox" name="check2" id="gridCheck">
							<label class="form-check-label" for="gridCheck">
							Doy mi consentimiento para el tratamiento de mis datos según lo indicado anteriormente y acepto la Política de Privacidad y el Aviso legal- condiciones de uso y compra.
							</label>
						</div>
					</div>
					<span style="color:red"> 
					@if($errors->first('check1') or $errors->first('check2'))
                    	<i class="fa fa-exclamation-circle"></i>
						Por favor aceptar el consentimiento para enviar el mensaje!
                    @endif
				</span>
				</div>
				<div class="section clearfix"></div>
				<div class="col-md-8 mt-3 ajax-form text-center">
					<button class="send_message" id="send" data-lang="en"><span>{{trans('messages.enviar')}}</span></button>
				</div>
		    </form>
				<div class="section clearfix"></div>
				<div class="col-md-8 padding-top-bottom">
					<div class="sep-line"></div>
				</div>
				<div class="section clearfix"></div>

				<div class="col-md-4 col-lg-3">
					<div class="address">
                        <div class="address-in text-left">
							<p class="color-black">{{trans('messages.correo')}}:</p>
						</div>
						<div class="address-in text-right">
							<p>info@hotelfarsund.es</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-lg-3">
					<div class="address">
						<div class="address-in text-left">
							<p class="color-black">{{trans('messages.telefono')}}:</p>
						</div>
						<div class="address-in text-right">
							<p>+34 986 72 90 00</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-lg-3">
					<div class="address">
					<div class="address-in text-left">
							<p class="color-black">{{trans('messages.direccion')}}:</p>
						</div>
						<div class="address-in text-right">
							<p >C/ Playa Areas, 54, 36966 Sanxenxo, Pontevedra, España</p>
						</div>
					</div>
				</div>
				<div class="section clearfix"></div>
				<div class="col-md-8 text-center mt-5 map-desktop" data-scroll-reveal="enter bottom move 50px over 0.7s after 0.2s" style="position: relative;right: 96px;">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2946.5786828194973!2d-8.777828684542628!3d42.39412097918456!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd42279311d46f91%3A0x1e1c7c1887f36fbd!2sHotel%20Farsund!5e0!3m2!1ses-419!2sve!4v1639109903853!5m2!1ses-419!2sve" width="950" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
				</div>

				<div class="col-md-8 text-center mt-5 map-mobile" data-scroll-reveal="enter bottom move 50px over 0.7s after 0.2s">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2946.5786828194973!2d-8.777828684542628!3d42.39412097918456!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd42279311d46f91%3A0x1e1c7c1887f36fbd!2sHotel%20Farsund!5e0!3m2!1ses-419!2sve!4v1639109903853!5m2!1ses-419!2sve" width="330" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
				</div>

				
				
				
			</div>
		</div>	
	</div>
	
@endsection