@extends('layouts.app')
@section('css')
<style>
    .input-date {
        position: relative;
        display: block;
        margin: 0;
        padding: 0;
        padding-left: 15px;
        padding-top: 9px;
        padding-bottom: 10px;
        width: 100%;
        font-family: 'Poppins', sans-serif;
        font-size: 13px;
        line-height: 1.2;
        letter-spacing: 1px;
        font-weight: 300;
        text-align: left;
        color: #fff;
        border: solid 1px rgba(255, 255, 255, .1);
        outline: none;
        background-color: #7c7c7c;
        transition: border-color .2s ease-out;
        background-color: transparent;
        border: solid 1px rgba(255, 255, 255, .1);
    }

    ::-webkit-calendar-picker-indicator {
        filter: invert(1);
    }

</style>
@endsection
@section('content')
<div class="section big-55-height over-hide">

    <div class="parallax parallax-top" style="background-image: url('../img/book.jpg')"></div>
    <div class="dark-over-pages"></div>

    <div class="hero-center-section pages">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 parallax-fade-top">
                    <div class="hero-text">{{trans('messages.reservas')}}</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section padding-top-bottom z-bigger">
    <div class="section z-bigger">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mt-4 mt-lg-0">
                    <div class="section">
                        <div class="customNavigation rooms-sinc-1-2">
                            <a class="prev-rooms-sync-1"></a>
                            <a class="next-rooms-sync-1"></a>
                        </div>
                        <div id="rooms-sync1" class="owl-carousel">
                            <div class="item">
                                <img src="{{ asset('img/room3.jpg') }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset('img/room4.jpg') }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset('img/room5.jpg') }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset('img/room6.jpg') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="section">
                        <div id="rooms-sync2" class="owl-carousel">
                            <div class="item">
                                <img src="{{ asset('img/room3.jpg') }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset('img/room4.jpg') }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset('img/room5.jpg') }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset('img/room6.jpg') }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset('img/room6.jpg') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="section pt-5">
                        <h5>{{trans('messages.habitacion')}}</h5>
                        <p class="mt-3" style="font-size:18px;font-weight:bold;color:#4B4B4B">{{$room->name}}
                        </p>
                    </div>
                    <div class="section pt-4">
                        <div class="row">
                            <div class="col-12">
                                <h6>{{trans('messages.descripcion')}}</h6>
                                <p class="mt-3">{{$room->description}}</p>
                            </div>
                            <div class="col-12"><br></div>
                            <div class="col-lg-6">
                                <p class="service-img-booking">
                                    <strong class="color-black">Ocupacion:</strong>
                                    Adultos {{$room->adult}}
                                    Niños:{{$room->children}}
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <p class="service-img-booking">
                                    <strong class="color-black">Precio:</strong>
                                    {{$room->price}}€
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <p class="service-img-booking">
                                    <strong class="color-black">Check-in:</strong>
                                    {{Carbon\Carbon::parse($room->check_in)->isoFormat('h:mm:ss a')}}
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <p class="service-img-booking">
                                    <strong class="color-black">Total:</strong>
                                    {{$total}}€
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <p class="service-img-booking">
                                    <strong class="color-black">Check-out:</strong>
                                    {{Carbon\Carbon::parse($room->check_out)->isoFormat('h:mm:ss a')}}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="section pt-4">
                        <div class="row">
                            <div class="col-6">
                                <h6>Servicios Inculidos</h6>
                                <br>
                                <p class="service-img-booking">
                                   
                                    <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                    Desayuno 5€<br>
                                    
                                </p>
                            </div>

                            <!-- <div class="col-6">
                                <h6>Servicios (Opcional)</h6>
                                <br>
                                <p class="service-img-booking">
                                    <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                    Desayuno 10€<br>
                                </p>
                            </div> -->

                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-4 order-first order-lg-last">
                    <div class="section background-dark p-4">
                        <form action="{{route('payment', $room->id)}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 pt-4">
                                            <input type="date" name="check_in" id="check_in"
                                                class="input-date {{ $errors->has('check_in') ? 'is-invalid' : '' }}"
                                                min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")));?>"
                                                value="{{!empty($chechin) ? $chechin : ''}}" />
                                        </div>
                                        <div class="col-12 pt-4">
                                            <input type="date" name="check_out" id="check_out"
                                                class="input-date {{ $errors->has('check_out') ? 'is-invalid' : '' }}"
                                                min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")));?>"
                                                value="{{!empty($checkout) ? $checkout : ''}}" />
                                        </div>
                                        <div class="col-12 pt-4">
                                            <input type="text" name="firstname"
                                                class="input-text {{ $errors->has('firstname') ? 'is-invalid' : '' }}"
                                                autocomplete="off" placeholder="Nombre"
                                                value="{{ Auth::check() ? auth()->user()->firstname : '' }}">
                                        </div>
                                        <div class="col-12 pt-4">
                                            <input type="text" name="lastname"
                                                class="input-text {{ $errors->has('lastname') ? 'is-invalid' : '' }}"
                                                autocomplete="off" placeholder="Apellido"
                                                value="{{ Auth::check() ? auth()->user()->lastname : '' }}">
                                        </div>
                                        <div class="col-12 pt-4">
                                            <input type="text" name="dni"
                                                class="input-text {{ $errors->has('dni') ? 'is-invalid' : '' }}"
                                                autocomplete="off" placeholder="DNI"
                                                value="{{ Auth::check() ? auth()->user()->dni : '' }}">
                                        </div>
                                        <div class="col-12 pt-4">
                                            <input type="email" name="email"
                                                class="input-text {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                                autocomplete="off" placeholder="Correo Electronico"
                                                value="{{ Auth::check() ? auth()->user()->email : '' }}">
                                        </div>
                                        <div class="col-12 pt-4">
                                            <input type="number" name="phone"
                                                class="input-text {{ $errors->has('phone') ? 'is-invalid' : '' }}"
                                                autocomplete="off" placeholder="Telefono"
                                                value="{{ Auth::check() ? auth()->user()->phone : '' }}">
                                        </div>                                        
                                    </div>
                                </div>
                                <div class="col-12 pt-4">
                                    <button class="booking-button">{{trans('messages.reservar_ahora')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if (session('status'))
<!-- Modal -->
<div class="modal fade modal-booking" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" style="padding-top:350px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Reserva</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span style="color:#777777;font-weight:bold">{{ session('status') }}</span>
            </div>
        </div>
    </div>
</div>
@endif

@endsection


@section('js')
@if (session('status'))
<script>
    $(document).ready(function () {
        $('.modal-booking').modal('show');
    });
</script>
@endif

<script>
    $(document).ready(function () {
        let start = document.getElementById('start');
        start.min = (new Date()).toISOString().substr(0, 18);
        let end = document.getElementById('end');
        end.min = (new Date()).toISOString().substr(0, 18);
    });

</script>
@endsection
