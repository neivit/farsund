@extends('layouts.app')
@section('css')
<style>
    .row {
        margin-bottom: 15px;
    }

</style>
@endsection
@section('content')
<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>

<div class="section z-bigger" style="padding-top:60px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="subtitle with-line text-center mb-4">{{trans('messages.registro')}}</div>
                <h3 class="text-center padding-bottom-small">{{trans('messages.usuario')}}</h3>
            </div>
            <div class="section clearfix"></div>
        </div>

        <div class="row justify-content-center padding-bottom-smaller">
            <form method="post" action="{{route('store.user')}}">
                @csrf
                <div class="row">
                    <div class="col">
                        <input type="text" name="firstname"
                            class="form-control {{ $errors->has('firstname') ? 'is-invalid' : '' }}"
                            placeholder="Nombre" autocomplete="off" value="{{old('firstname')}}">
                    </div>
                    <div class="col">
                        <input type="text" name="lastname"
                            class="form-control {{ $errors->has('lastname') ? 'is-invalid' : '' }}"
                            placeholder="Apellido" autocomplete="off" value="{{old('lastname')}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="number" name="dni"
                            class="form-control {{ $errors->has('dni') ? 'is-invalid' : '' }}" placeholder="DNI"
                            autocomplete="off" value="{{old('dni')}}">
                    </div>
                    <div class="col">
                        <input type="email" name="email"
                            class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                            placeholder="Correo electrónico" autocomplete="off" value="{{old('email')}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="number" name="phone"
                            class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}"
                            placeholder="Telefono movil" autocomplete="off" value="{{old('phone')}}">
                    </div>
                    <div class="col">
                        <input type="password" name="password"
                            class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                            placeholder="Contraseña">
                    </div>
                </div>
                <div class="row  justify-content-center">
                    <div class="section clearfix"></div>
                    <div class="col-md-8 mt-3 ajax-form text-center">
                        <button class="send_message" id="send"
                            data-lang="en"><span>{{trans('messages.guardar')}}</span></button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row justify-content-center padding-bottom-smaller"></div>
        <div class="row justify-content-center padding-bottom-smaller"></div>
        <div class="row justify-content-center padding-bottom-smaller"></div>

    </div>
</div>

@endsection
