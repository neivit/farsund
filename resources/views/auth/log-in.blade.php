@extends('layouts.app')
@section('content')
<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>

<div class="section padding-top z-bigger">
    <div class="container">
        <div class="row justify-content-center padding-bottom-smaller">
            <div class="col-md-8">
                <h3 class="text-center padding-bottom-small">{{trans('messages.session')}}</h3>
            </div>
            <div class="section clearfix"></div>
        </div>


        <div class="row justify-content-center padding-bottom-smaller">
            <form method="post" action="{{route('authentication')}}">
                @csrf
                <div class="row">
                    <div class="col">
                        <input type="text" name="email"
                            class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                            placeholder="{{trans('messages.email')}}" autocomplete="off">
                    </div>
                    <div class="col">
                        <input type="password" name="password"
                            class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                            placeholder="{{trans('messages.contraseña')}}">
                    </div>
                </div>

                <div class="row justify-content-center padding-bottom-smaller">
                    <div class="section clearfix"></div>
                    <div class="col-md-8 mt-3 ajax-form text-center">
                        <button class="send_message" id="send"
                            data-lang="en"><span>{{trans('messages.session')}}</span></button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row justify-content-center padding-bottom-smaller"></div>
        <div class="row justify-content-center padding-bottom-smaller"></div>
        <div class="row justify-content-center padding-bottom-smaller"></div>

    </div>
</div>



@endsection
