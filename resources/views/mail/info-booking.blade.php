<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="x-apple-disable-message-reformatting">
	<title></title>
	<!--[if mso]>
	<noscript>
		<xml>
			<o:OfficeDocumentSettings>
				<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
	</noscript>
	<![endif]-->
	<style>
		table, td, div, h1, p {font-family: Arial, sans-serif;}
	</style>
</head>
<body style="margin:0;padding:0;">
	<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
		<tr>
			<td align="center" style="padding:0;">
				<table role="presentation" style="width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
					<tr>
						<td align="left" style="padding:25px 0 20px 0;background:#212529f4;">
                            <img src="https://hotelfarsund.com/img/logo/logo.png" width="100px;" style="margin-left: 15px;">
						</td>
					</tr>
					<tr>
						<td style="padding:36px 30px 42px 30px;">
							<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
								<tr>
									<td>
										<h1 style="color:#555556;margin-left:15px;font-size:24px;font-family:Arial,sans-serif;">Su reserva está confirmada</h1>
									</td>
								</tr>
								<tr>
									<td>
										<center><img src="https://hotelfarsund.com/img/room3.jpg" width="500px;"></center>
									</td>
								</tr>
								<tr style="border-bottom:1px solid #e3e3e3">
									<td>
										<h4 style="color:#555556;margin-left:15px;">{{$room}}</h4>
									</td>
								</tr>
								<tr style="border-bottom:1px solid #e3e3e3;">
									<td colspan="2">
										<div style="color:#555556;margin-left:15px;margin-top:10px;line-height: 1.2;float:left;">
											{{$dayStart}},<br>
											{{$dateStart}}<br>
											Check-in
											{{$hourStart}}
										</div>
										<div style="color:#555556;margin-left:15px;margin-top:10px; margin-bottom:10px;line-height: 1.2; float:right">
											{{$dayEnd}},<br>
											{{$dateEnd}}<br>
											Check-out
											{{$hourEnd}}
										</div>
									</td>
								</tr>
								<tr style="border-bottom:1px solid #e3e3e3;">
									<td colspan="2">
										<div style="color:#555556;margin-left:15px;margin-top:10px;margin-bottom:10px;line-height: 1.2;float:left;">
											Dirección,<br>
											C/ Playa Areas, 54, 36966 Sanxenxo, Pontevedra, España
										</div>
									</td>
								</tr>
								<tr style="border-bottom:1px solid #e3e3e3;">
                                    <td colspan="2">
										<div style="color:#555556;margin-left:15px;margin-top:10px;line-height: 1.2;float:left;">
                                            Precio<br>
											€{{$price}}
										</div>
									</td>
								</tr>
								
								
							</table>
						</td>
					</tr>
					<tr>
						<td style="padding:30px;background:#dcbc74;">
							<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;font-size:9px;font-family:Arial,sans-serif;">
								<tr>
									<td  align="left">
										<p style="margin:0;font-size:14px;line-height:16px;font-family:Arial,sans-serif;color:#ffffff;">
											Copyright © Hotel farsund, 2022 
										</p>
									</td>
									
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>

