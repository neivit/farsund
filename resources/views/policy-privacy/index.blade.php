@extends('layouts.app')
@section('css')
<style>
    td p {
        color: #000;
    }

    p {
        color: #000;
    }

</style>
@endsection
@section('content')
<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>

<div class="section padding-top z-bigger">
    <div class="container">
        <div class="row justify-content-center padding-bottom-smaller">
            <div class="col-md-8">
                <h4 class="text-center padding-bottom-small">POLÍTICA DE PRIVACIDAD Y COOKIES</h4>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th style="border:none;" scope="col">
                            <p style="font-weight:bold;">Política de Privacidad</p>
                        </th>
                    </tr>
                </thead>
            </table>


            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">EPÍGRAFE</th>
                        <th scope="col">INFORMACIÓN BÁSICA</th>
                        <th scope="col">INFORMACIÓN ADICIONAL</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td>
                            <p style="font-weight:bold;">¿Quién es el responsable?</p>
                        </td>
                        <td>
                            <p>JOSÉ LUIS OTERO RODRÍGUEZ</p>
                        </td>
                        <td>
                            <p><strong>Teléfono:</strong> 986 729 000</p>
                            <p><strong>Domicilio:</strong> Playa Areas, 54 – 36966, Sanxenxo (Pontevedra)</p>
                            <p><strong>E-mail:</strong> farsundhotel@gmail.com</p>
                            <p><strong>NIF:</strong> 76824177- Z</p>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-weight:bold;">¿Cuál es la finalidad del conjunto de tratamientos?</p>
                        </td>
                        <td>
                            <p>
                                Esta web tiene una finalidad informativa y de contacto, por lo que los únicos datos que
                                se tratarán serán los necesarios para poder contactar con los usuarios que así lo hayan
                                requerido y así poder gestionar correctamente una futura reserva de alojamiento.
                            </p>
                        </td>
                        <td>
                            <p style="font-weight:bold;">Plazos o criterios de conservación de datos:</p>
                            <p>
                                El conjunto de datos de nuestros huéspedes será conservado durante el tiempo por el que
                                transcurra su relación comercial con nuestra entidad (tiempo de alojamiento) y, en todo
                                caso, hasta que el cliente solicite su supresión.</p>
                            <p>Algunos datos derivados de la relación contractual entre la empresa y el cliente serán
                                conservados durante el tiempo límite fijado por la legislación aplicable a efectos de
                                atender posibles requerimientos de organismos público o de Cuerpos y Fuerzas de
                                Seguridad del Estado.</p>

                    </tr>
                    <tr>
                        <td>
                            <p style="font-weight:bold;">¿Cuál es la base que legitima los tratamientos?</p>
                        </td>
                        <td>
                            <p style="font-weight:bold;">La base jurídica del tratamiento es:</p>
                            <p>
                                Para las comunicaciones vía web la base de legitimación es el consentimiento expreso del
                                interesado, mientras que para la gestión de las reservas de alojamiento la base de
                                legitimación será el cumplimiento del contrato de prestación de servicios de
                                alojamiento.
                            </p>
                        </td>
                        <td>
                            <p style="font-weight:bold;">Obligación de no de facilitar datos y consecuencias de no
                                hacerlo:</p>
                            <p>
                                Los datos requeridos a nuestros huéspedes y futuros huéspedes son los estrictamente
                                necesarios para mantener una adecuada comunicación con ellos y una correcta gestión
                                administrativa de sus reservas.
                            </p>
                            <p>A estos efectos, al requerir únicamente los datos mínimos para la finalidad indicada, la
                                negativa a proporcionar alguno de ellos originará la imposibilidad de prestar el
                                servicio de alojamiento.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-weight:bold;">¿A quién proporcionamos sus datos?</p>
                        </td>
                        <td colspan="2">
                            <p>
                                Todos los datos manejados por nuestra entidad serán manipulados por el personal, no
                                siendo cedidos a terceros no autorizados para su conocimiento. </p>
                            <p>
                                No obstante, por obligación legal, los datos identificativos de nuestros huéspedes
                                podrán ser cedidos a los Cuerpos y Fuerzas de Seguridad del Estado y, por razón del
                                servicio prestado, las plataformas online que puedan comercializar nuestros servicios
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-weight:bold;">¿Qué derechos tiene usted?</p>
                        </td>
                        <td>
                            <p>
                                El interesado podrá ejercer los derechos de acceso, rectificación, supresión, oposición,
                                limitación del tratamiento, y/o portabilidad de sus datos.
                            </p>
                        </td>
                        <td>
                            <ul>
                                <li>
                                    <p>Puede ejercer sus derechos mediante correo electrónico, correo postal o
                                        presencialmente.<p>
                                </li>
                                <li>
                                    <p>Tiene derecho a retirar el consentimiento prestado en cualquier momento, sin
                                        motivación alguna.<p>
                                </li>
                                <li>
                                    <p>Igualmente, tiene derecho a reclamar ante la autoridad de control (AEPD –
                                        www.aepd.es).<p>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-weight:bold;">¿De dónde obtenemos sus datos? ¿Cuál es su procedencia?</p>
                        </td>
                        <td colspan="2">
                            <p>
                                Todos los datos de nuestros huéspedes y futuros huéspedes son obtenidos directamente del
                                interesado usuario del Sitio web.
                            </p>
                        </td>
                    </tr>

                </tbody>
            </table>


            <table class="table">
                <thead>
                    <tr>
                        <th style="border:none;" scope="col">
                            <p style="font-weight:bold;">2. Uso de cookies</p>
                        </th>
                    </tr>
                </thead>
            </table>

            <p>La presente política de cookies tiene por finalidad informar al usuario de manera clara y precisa sobre
                las cookies que se utilizan en la página web
                titularidad de JOSÉ LUIS OTERO RODRÍGUEZ.</p>
            <p style="font-weight:bold;">¿Qué es una cookie?</p>
            <p>Una cookie es un pequeño archivo que se descarga en su ordenador al acceder a determinadas páginas web,
                normalmente formado por letras y números, enviado por el servidor de nuestra web al archivo de cookies
                de su navegador. Las cookies permiten a nuestra página web, entre otras cosas, almacenar y recuperar
                información sobre sus hábitos de navegación o de su equipo y recordar su presencia al establecerse una
                nueva conexión entre nuestro servidor y su navegador, así como para ofrecer un correcto funcionamiento
                del Sitio.</p>
            <p style="font-weight:bold;">¿Qué tipo de cookies emplea este Sitio?</p>




        </div>
        <p> <strong style="font-weight:bold;">1.)La Web emplea este tipo de cookies en este Sitio según el tiempo de
                permanencia:</strong> </p>
        <p>
            - Cookies persistentes: favorecen la experiencia como usuario (por ejemplo, ofreciendo una navegación
            personalizada). Estas cookies permanecen en su navegador durante un periodo de tiempo más amplio. Este
            periodo depende de los ajustes que haya introducido en su navegador. Las cookies persistentes permiten que
            la información sea transferida al servidor de este Sitio cada vez que lo visita. También se conocen como
            cookies de seguimiento.
        </p>
        <p> <strong style="font-weight:bold;">2.) Según la finalidad para la que se tratan los datos obtenidos, este
                Sitio emplea los siguientes tipos de cookies:</strong> </p>
        <p>
            - Cookies de personalización: Son aquéllas que le permiten acceder al servicio con algunas características
            de carácter general predefinidas en función
            de una serie de criterios en el terminal (por ejemplo, el idioma, el tipo de navegador a través del cual
            accede al servicio, la configuración regional
            desde donde accede al servicio, etc.).
        </p>
        <p>
            - Cookies de análisis: Son aquéllas que bien tratadas por nosotros o por terceros, nos permiten cuantificar
            el número de usuarios y así realizar la
            medición y análisis estadístico de la utilización que hacen los usuarios del servicio ofertado. Para ello se
            analiza su navegación en nuestra página web
            con el fin de mejorar la oferta de productos o servicios que le ofrecemos.
        </p>
        <p>
            - Cookies de publicidad comportamental: Son aquéllas que, bien tratadas por nosotros o por terceros, nos
            permiten la gestión, de la forma más eficaz
            posible, de los espacios publicitarios que, en su caso, pudieran estar incluidos en este Sitio desde el que
            se presta el servicio solicitado. Estas cookies
            almacenan información de su comportamiento obtenida a través de la observación continuada de sus hábitos de
            navegación, lo que nos permite
            desarrollar un perfil específico para mostrar la publicidad en función de este.
        </p>
        <p><strong style="font-weight:bold;">3.) Según su procedencia, este Sitio emplea este tipo de cookies:</strong>
        </p>
        <p>- Cookies de terceros: Son aquellas que se envían al equipo terminal del usuario desde un equipo o dominio
            que no es gestionado por el editor, sino
            por otra entidad que trata los datos obtenidos través de las cookies.</p>


        <p style="text-align:center;">Cookies de terceros usadas en la web: <strong
                style="color:red;background-color:yellow">A revisar por los gestores de la web!</strong></p>
        <p style="text-align: center;"><img src="{{asset('img/cookiestable.png')}}"></p>

        <table class="table">
            <thead>
                <tr>
                    <th style="border:none;" scope="col">
                        <p style="font-weight:bold;">4. Seguimiento y Análisis</p>
                    </th>
                </tr>
            </thead>
        </table>

        <p>Para la continua optimización de sus comunicaciones de marketing, el Sitio web emplea el software analítico
            Google Analytics. Esta tecnología
            permite conocer el comportamiento online del visitante en términos de tiempo, localización geográfica y uso
            de este Sitio. La información se recoge
            a través de pixel tags y/o cookies de seguimiento. Esta información es anónima, no es vinculada con los
            datos personales y no se comparte con terceros
            para su uso independiente. Toda la información necesaria para el análisis es almacenada en los servidores en
            Google.</p>

        <table class="table">
            <thead>
                <tr>
                    <th style="border:none;" scope="col">
                        <p style="font-weight:bold;">5. Habilitación e inhabilitación de las cookies y de recursos
                            similares</p>
                    </th>
                </tr>
            </thead>
        </table>

        <p>
            Recuerde que ciertas funciones y la plena funcionalidad de este Sitio pueden no estar disponibles después de
            deshabilitar las cookies.
        </p>
        <p>
            Puede usted permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de
            las opciones del navegador instalado en su
            ordenador:
        </p>
        <p style="text-align: center;"><img src="{{asset('img/cookieslist.png')}}"></p>
        <table class="table">
            <thead>
                <tr>
                    <th style="border:none;" scope="col">
                        <p style="font-weight:bold;">6. Cambios en nuestra Política de Privacidad</p>
                    </th>
                </tr>
            </thead>
        </table>
        <p>Cualquier cambio que en el futuro podamos introducir en nuestra Política de Privacidad será comunicado en
            esta página. De este modo, el usuario
            podrá comprobar regularmente las actualizaciones o cambios en nuestra Política de Privacidad.</p>
        <br> <br><br>
    </div>
</div>
@endsection
