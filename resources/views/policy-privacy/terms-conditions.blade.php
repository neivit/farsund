@extends('layouts.app')
@section('css')
  <style>
      .title-resumen{
        font-weight: bold;
        color: black;
      }
      p{
          color:black;
      }
  </style>
@endsection
@section('content')
<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>

<div class="section padding-top z-bigger">
    <div class="container">
        <div class="row justify-content-center padding-bottom-smaller">
            <div class="col-md-8">
                <h4 class="text-center padding-bottom-small">AVISO LEGAL</h4>
            </div>
		   <table class="table">
                <thead>
                    <tr>
                        <th style="border:none;" scope="col">
                        <p style="font-weight:bold;text-decoration:underline;color:black">Responsable</p>
                        <p><span class="title-resumen">Titular de la Web:</span> JOSÉ LUIS OTERO RODRÍGUEZ</p>
                        <p><span class="title-resumen">NIF:</span> 76824177-Z</p>
                        <p><span class="title-resumen">Teléfono:</span> 986 729 000</p>
                        <p><span class="title-resumen">E-mail:</span> farsundhotel@gmail.com</p>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>

        <p>
        Esta página web es propiedad de JOSÉ LUIS OTERO RODRÍGUEZ (en adelante, el
        PROPIETARIO), con NIF nº 76824177-Z y domicilio en Playa Areas, 54 – 36966,
        Sanxenxo (Pontevedra).
        </p>
        
        <p>Para cualquier consulta o propuesta, contáctenos en el <span style="color:#25698B; font-weight:bold">e-mail: farsundhotel@gmail.com</span></p>
        
        <p>
            La página web se rige por la normativa exclusivamente aplicable en España y en el
            espacio que comprende la Unión Europea, quedando sometidos a ella tanto nacionales
            como extranjeros que utilicen esta web.
        </p>
        
        <p>
            El acceso a nuestra página web por parte del USUARIO es gratuito y está condicionado
            a la previa lectura y aceptación íntegra, expresa y sin reservas del presente Aviso Legal
            vigente en el momento del acceso, que rogamos lea detenidamente. El USUARIO, en el
            momento que utiliza nuestro portal y sus contenidos o servicios, acepta y se somete
            expresamente a las Condiciones Generales de uso de este. Si el USUARIO no estuviere
            de acuerdo con las presentes Condiciones de uso, deberá abstenerse de utilizar este portal
            y operar por medio del mismo.

        </p>
        
        <p>
            En cualquier momento podremos modificar la presentación y configuración de nuestra
            Web, ampliar o reducir servicios, e incluso suprimirla de la Red, así como los servicios y
            contenidos prestados, todo ello de forma unilateral y sin previo aviso.
        </p>
        
        <table class="table">
                <thead>
                    <tr>
                        <th style="border:none;" scope="col">
                        <p style="font-weight:bold;color:black">Propiedad Intelectual</p>
                        </th>
                    </tr>
                </thead>
        </table>
        
        <p>
            Todos los contenidos, textos, imágenes, marcas y códigos fuente son de nuestra propiedad
            o de terceros a los que se han adquirido sus derechos de explotación, y están protegidos
            por los derechos de Propiedad Intelectual e Industrial.
        </p>
        
        <p>
            El usuario únicamente tiene derecho a un uso privado de los mismos, sin ánimo de lucro,
            y necesita autorización expresa para modificarlos, reproducirlos, explotarlos, distribuirlos
            o ejercer cualquier derecho perteneciente a su titular.
        </p>
        
        <table class="table">
                <thead>
                    <tr>
                        <th style="border:none;" scope="col">
                        <p style="font-weight:bold;color:black">Condiciones Generales de acceso y uso</p>
                        </th>
                    </tr>
                </thead>
        </table>
        
        <p>
            El acceso a nuestra página web es gratuito y no exige previa suscripción o registro. La
            finalidad de esta es meramente informativa o publicitaria. De cualquier modo, se ofrece
            un formulario de contacto para que pueda solicitar más información.
        </p>
        
        <p>
            El envío de datos personales a través del formulario de contacto implicará la aceptación
            expresa por parte del USUARIO de nuestra política de privacidad, con la habilitación del
            check respectivo en el momento del envío de sus datos.
        </p>
        
        <p>
            El USUARIO debe acceder a la misma conforme a la buena fe, las normas de orden
            público y a las presentes Condiciones Generales de uso. El acceso a nuestro sitio web se
            realiza bajo la propia y exclusiva responsabilidad del USUARIO, que se abstendrá de
            utilizar cualquiera de los servicios con fines o efectos ilícitos, prohibidos o lesivos para
            los derechos de terceras personas, respondiendo en todo caso de los daños y perjuicios
            que pueda causar a terceros o a nosotros mismos.
        </p>
        
        <p>
            Teniendo en cuenta la imposibilidad de control respecto a la información, contenidos y
            servicios que contengan otras páginas web a los que se pueda acceder a través de los
            enlaces que nuestra página web pueda poner a su disposición, le comunicamos que
            quedamos eximidos de cualquier responsabilidad por los daños y perjuicios de toda clase
            que pudiesen derivar por la utilización de esas páginas web, ajenas a nuestra empresa, por
            parte del USUARIO.
        </p>
        
        <p>
            Puede obtener más información sobre el uso que hacemos de sus datos de carácter
            personal en la <a href="{{route('policy.privacy')}}">Política de Privacidad</a> de nuestra web.
        </p>
        
        <table class="table">
                <thead>
                    <tr>
                        <th style="border:none;" scope="col">
                        <p style="font-weight:bold;color:black">Responsabilidad</p>
                        </th>
                    </tr>
                </thead>
        </table>
        <p>El PROPIETARIO en ningún caso será responsable de:</p>
        <ul>
            <li>
                <p>Los fallos e incidencias que pudieran producirse en las comunicaciones, borrado
                o transmisiones incompletas de manera que no se garantiza que los servicios del
                sitio web estén constantemente operativos.</p>
            </li>
            <li>
                <p>De la producción de cualquier tipo de daño que los usuarios o terceros pudiesen
                   ocasionar en el sitio web.</p>
            </li>
            <li>
                <p>De la fiabilidad y veracidad de las informaciones introducidas por terceros en el
                   sitio web, bien directamente, bien a través de enlaces o links.</p>
            </li>
        </ul>
        <p>
          El PROPIETARIO se reserva el derecho a suspender el acceso sin previo aviso de forma
          discrecional y con carácter definitivo o temporal hasta el aseguramiento de la efectiva
          responsabilidad de los daños que pudieran producirse.
        </p>
        <p>
          El PROPIETARIO colaborará y notificará a la autoridad competente las incidencias
          anteriormente indicadas en el momento en que tenga conocimiento fehaciente de que los
          daños ocasionados constituyan cualquier tipo de actividad ilícita.
        </p>
        <br><br><br>
    </div>
</div>
@endsection
