@extends('layouts.app')
 @section('content')
	<div class="section big-55-height over-hide z-bigger">
	
		<div class="parallax parallax-top" style="background-image: url('img/services.jpg')"></div>
		<div class="dark-over-pages"></div>
	
		<div class="hero-center-section pages">
			<div class="container">
				<div class="row justify-content-center">
				</div>
			</div>
		</div>
	</div>
	
	<div class="section padding-top-bottom-smaller background-dark-2 over-hide" style="padding-top:50px">		
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">	
					
                
		<div class="section z-bigger">		
			<div class="container">
				<div class="row">
					<div class="col-6 col-sm-6 col-md-4 col-lg-2 text-center">
						<div class="amenities">
							<img src="img/icons/1.png" alt="">
							<p>{{trans('messages.estacionamiento')}}</p>
						</div>
					</div>
					<div class="col-6 col-sm-6 col-md-4 col-lg-2 text-center">
						<div class="amenities">
							<img src="img/icons/2.png" alt="">
							<p>{{trans('messages.playa')}}</p>
						</div>
					</div>
					<div class="col-6 col-sm-6 col-md-4 col-lg-2 text-center mt-4 mt-md-0">
						<div class="amenities">
							<img src="img/icons/3.png" alt="">
							<p>{{trans('messages.wifi')}}</p>
						</div>
					</div>
					<div class="col-6 col-sm-6 col-md-4 col-lg-2 text-center mt-4 mt-lg-0">
						<div class="amenities">
							<img src="img/icons/4.png" alt="">
							<p>{{trans('messages.restaurante')}}</p>
						</div>
					</div>
					<div class="col-6 col-sm-6 col-md-4 col-lg-2 text-center mt-4 mt-lg-0">
						<div class="amenities">
							<img src="img/icons/5.png" alt="">
							<p>{{trans('messages.bar')}}</p>
						</div>
					</div>
					<div class="col-6 col-sm-6 col-md-4 col-lg-2 text-center mt-4 mt-lg-0">
						<div class="amenities">
							<img src="img/icons/6.png" alt="">
							<p>{{trans('messages.comida_incluida')}}</p>
						</div>
					</div>
				</div>
			</div>					
		</div>
	

				</div>
			</div>
		</div>	
	</div>
	
	<div class="section padding-top-bottom background-grey over-hide">
		<div class="container">
			<div class="row justify-content-center padding-bottom-small">
				<div class="col-md-8 align-self-center ">
					<h3 class="text-center">{{trans('messages.servicios')}}</h3>
					<div class="subtitle with-line text-center mb-4 ">
						@foreach($hotelServices as $service)
						<div class="amenities">
							<img src="img/services/carta.svg" alt="">
						</div>  
						<a style="color:#363636 !important;text-align:center;position:relative;top:8px;" target="_blank"t href="{{asset('files/'.$service->file)}}">Descargar carta</a>	
						@endforeach
					</div>
				</div>
				<div class="section clearfix"></div>
			</div>
			<div class="row background-white p-0 m-0">
				<div class="col-xl-6 p-0">
					<div class="img-wrap" id="rev-1">
						<img src="img/exterior.jpg" alt="">
					</div>
				</div>
				<div class="col-xl-6 p-0 align-self-center">
					<div class="row justify-content-center">
						<div class="col-9 pt-4 pt-xl-0 pb-5 pb-xl-0 text-center">
							<h5 class="" style="text-align:center;margin-left:16px;">{{trans('messages.exteriores')}}</h5>
							<p class="mt-3">
								<ul style="text-align:center;list-style:none;margin-right:29px">
									<li>Mobiliario exterior</li>
									<li>Situado frente a la playa</li>
									<li>Terraza / solárium</li>
									<li>Barbacoa</li>
									<li>Terraza</li>
									<li>Jardín</li>
								</ul>
							</p>
						</div>
					</div>	
				</div>
			</div>
			<div class="row background-white p-0 m-0">
				<div class="col-xl-6 p-0 align-self-center">
					<div class="row justify-content-center">
						<div class="col-9 pt-4 pt-xl-0 pb-5 pb-xl-0 text-center">
							<h5 class="" style="text-align:center;">{{trans('messages.comida_bebida')}}</h5>
							<p class="mt-3">
								<ul style="text-align:center;list-style:none;margin-right:33px;">
									<li>Cafetería en el alojamiento</li>
									<li>Botella de agua</li>
									<li>Vino / champán </li>
									<li>Buffet para niños</li>
									<li>Bar / Restaurante</li>
									<li>Fruta</li>
								</ul>
							</p>
							
						</div>
					</div>
				</div>
				<div class="col-xl-6 order-first order-xl-last p-0">
					<div class="img-wrap" id="rev-2">
						<img src="img/comida_bebida.jpg" alt="">
					</div>
				</div>
			</div>
			<div class="row background-white p-0 m-0">
				<div class="col-xl-6 p-0">
					<div class="img-wrap" id="rev-3">
						<img src="img/actividades.jpg" alt="">
					</div>
				</div>
				<div class="col-xl-6 p-0 align-self-center">
					<div class="row justify-content-center">
						<div class="col-9 pt-4 pt-xl-0 pb-5 pb-xl-0 text-center">
							<h5 class="">{{trans('messages.actividades')}}</h5>
							<p class="mt-3">
								<ul style="text-align:center;list-style:none;margin-right:33px;">
									<li>Campo de golf (a menos de 3 km)</li>
									<li>Ciclismo / Senderismo </li>
									<li>Ruta de bares</li>
									<li>Tours a pie</li>
									<li>Biblioteca</li>
									<li>Playa</li>
								</ul>
							</p>
						</div>
					</div>	
				</div>
			</div>
			<div class="row background-white p-0 m-0">
				<div class="col-xl-6 p-0 align-self-center">
					<div class="row justify-content-center">
						<div class="col-9 pt-4 pt-xl-0 pb-5 pb-xl-0 text-center">
							<h5 class="">{{trans('messages.general')}}</h5>
							<p class="mt-3">
							<ul style="text-align:center;list-style:none;margin-right:33px;">
									<li>Prohibido fumar en todo el alojamiento</li>		
									<li>Zona TV / salón de uso compartido</li>
									<li>Habitaciones para no fumadores</li>
									<li>Servicio de habitaciones</li>
									<li>Aire acondicionado</li>
									<li>Zona de fumadores</li>
									<li>Calefacción</li>
								</ul>
							</p>
						</div>
					</div>
				</div>
				<div class="col-xl-6 order-first order-xl-last p-0">
					<div class="img-wrap" id="rev-4">
						<img src="img/general.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>

 @endsection