@extends('layouts.app')
@section('content')

	<div class="section big-55-height over-hide">
	
		<div id="poster_background-explore" style="background-image: url('img/aboutHotelFarsund.jpg')"></div>	
		<div class="dark-over-pages"></div>
	
		<div class="hero-center-section pages">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 parallax-fade-top">
						<div class="hero-text">FARSUND</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="section padding-top-bottom over-hide">
		<div class="section z-bigger">		
			<div class="container">
				<div class="row">
					<div class="col-12">
						<p class="m-0">{{trans('messages.farsund_about')}}</p>
					</div>
				</div>
			</div>					
		</div>
	</div>
		
	<div class="section padding-bottom over-hide">
		<div class="section z-bigger">		
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="img-wrap">	
							<img src="img/about-1.jpg" alt="">					
						</div>
					</div>
					<div class="col-md-6 mt-4 mt-md-0">
						<div class="img-wrap">	
							<img src="img/about-2.jpg" alt="">					
						</div>
					</div>
					<div class="col-12 mt-4">
						<div class="img-wrap">	
							<img src="img/about-3.jpg" alt="">					
						</div>
					</div>
				</div>
			</div>					
		</div>
	</div>
@endsection