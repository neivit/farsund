﻿@extends('layouts.app')
@section('css')
<style>
    .input-date {
        position: relative;
        display: block;
        margin: 0;
        padding: 0;
        padding-left: 15px;
        padding-top: 9px;
        padding-bottom: 10px;
        width: 100%;
        font-family: 'Poppins', sans-serif;
        font-size: 13px;
        line-height: 1.2;
        letter-spacing: 1px;
        font-weight: 300;
        text-align: left;
        color: #fff;
        border: solid 1px rgba(255, 255, 255, .1);
        outline: none;
        background-color: #7c7c7c;
        transition: border-color .2s ease-out;
        background-color: transparent;
        border: solid 1px rgba(255, 255, 255, .1);
    }

    ::-webkit-calendar-picker-indicator {
        filter: invert(1);
    }
    .info{
        font-size: 12px;
    }

</style>
@endsection
@section('content')
<div class="section big-55-height over-hide z-bigger">

    <div class="parallax parallax-top" style="background-image: url('img/book.jpg')"></div>
    <div class="dark-over-pages"></div>

    <div class="hero-center-section pages">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 parallax-fade-top">
                    <div class="hero-text">{{trans('messages.reservas')}}</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section padding-top-bottom z-bigger">
    <div class="section z-bigger">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mt-4 mt-lg-0">
                    <div class="row">
                        @forelse($rooms as $obj)
                        <div class="col-lg-6">
                            <div class="room-box background-grey">
                                <div class="room-per">
                                    <span>€{{$obj->price}}</span>
                                </div>
                                <div style="position: relative;">
                                    <div class="customNavigation custom-rooms-sinc-1-2">
                                        <a id="button-prev-{{$obj->id}}" class="custom-prev-rooms-sync"></a>
                                        <a id="button-next-{{$obj->id}}" class="custom-next-rooms-sync"></a>
                                    </div>
                                    <div data-room="{{$obj->id}}" id="custom-rooms-sync-{{$obj->id}}"
                                        class="custom-owl-carousel owl-carousel">
                                        <div class="item">
                                            <img src="{{ asset('img/room3.jpg') }}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{ asset('img/room4.jpg') }}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{ asset('img/room5.jpg') }}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{ asset('img/room6.jpg') }}" alt="">
                                        </div>
                                    </div>
                                </div>

                                <div class="room-box-in">
                                    <h4 style="font-size:20px">{{ $obj->name }}</h4>
                                    <p class="mt-3">{{$obj->description}}</p>
                                    @if($obj->refundable)
                                    <p class="mt-3 info">
                                        <i class="fa fa-check" aria-hidden="true"></i> Reembolsable {{$obj->refundable_num}} dias
                                    </p>
                                    @endif
                                    @foreach($obj->restrictions as $restrinction)
                                    <p class="mt-3 info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i> Minimo de reserva {{$restrinction->min_book}}
                                    </p>
                                    @endforeach
                                    <div class="room-icons mt-4 pt-4">
                                        <!-- <img src="img/5.svg" alt="">
                                        <img src="img/2.svg" alt="">
                                        <img src="img/3.svg" alt="">
                                        <img src="img/1.svg" alt=""> -->
                                        <a href="{{route('book', [$obj->id, $checkIn, $checkOut])}}"
                                            style="font-weight:bold;color:#343a40">{{trans('messages.reservar')}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @empty
                        <div class="col-lg-6">
                            <div class="room-box background-grey">
                                <div class="room-per">
                                    <span></span>
                                </div>
                                <img src="img/empty.png" alt="">
                                <div class="room-box-in">
                                    <h5 class="">No disponible</h5>
                                    <p class="mt-3">No disponible</p>
                                    <div class="room-icons mt-4 pt-4">
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforelse
                    </div>
                    <br>
                </div>

                <div class="col-lg-4 order-first order-lg-last">
                    <div class="section background-dark p-4">
                        <form action="{{route('rooms')}}">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-item">
                                                <span class="fontawesome-calendar"></span>
                                                <input type="date" name="check_in" class="input-date"
                                                    value="{{!empty($checkIn) ? $checkIn : ''}}"
                                                    min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")));?>" />
                                                <span class="date-text date-depart"></span>
                                            </div>
                                        </div>
                                        <div class="col-12 pt-4">
                                            <div class="form-item">
                                                <span class="fontawesome-calendar"></span>
                                                <input type="date" name="check_out" class="input-date"
                                                    value="{{!empty($checkOut) ? $checkOut : ''}}"
                                                    min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")));?>" />
                                                <span class="date-text date-return"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 pt-4">
                                            <select name="adult" class="wide">
                                                <option value="0" {{ '' == $adult ? 'selected' : '' }}>
                                                    {{trans('messages.adultos')}}</option>
                                                <option value="1" {{ 1 == $adult ? 'selected' : '' }}>1</option>
                                                <option value="2" {{ 2 == $adult ? 'selected' : '' }}>2</option>
                                                <option value="3" {{ 3 == $adult ? 'selected' : '' }}>3</option>
                                                <option value="4" {{ 4 == $adult ? 'selected' : '' }}>4</option>
                                                <option value="5" {{ 5 == $adult ? 'selected' : '' }}>5</option>
                                            </select>
                                        </div>
                                        <div class="col-12 pt-4">
                                            <select name="children" class="wide">
                                                <option value="0" {{ '' == $children ? 'selected' : '' }}>
                                                    {{trans('messages.niños')}}</option>
                                                <option value="1" {{ 1 == $children ? 'selected' : '' }}>1</option>
                                                <option value="2" {{ 2 == $children ? 'selected' : '' }}>2</option>
                                                <option value="3" {{ 3 == $children ? 'selected' : '' }}>3</option>
                                                <option value="4" {{ 4 == $children ? 'selected' : '' }}>4</option>
                                                <option value="5" {{ 5 == $children ? 'selected' : '' }}>5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-12 pt-3">
                                    <ul class="list">
                                        @foreach($extras as $extra)
                                        <li class="list__item">
                                            <label class="label--checkbox">
                                                <input type="checkbox" class="checkbox" name="{{$extra->name}}">
                                                {{$extra->name}}
                                            </label>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <button type="submit" class="booking-button"
                                style="font-weight:bold">{{trans('messages.buscar')}}</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@if (session('status'))
<!-- Modal -->
<div class="modal fade modal-booking" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" style="padding-top:350px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Reserva</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span style="color:#777777;font-weight:bold">{{ session('status') }}</span>
            </div>
        </div>
    </div>
</div>
@endif

@if (session('status'))
<script>
    $(document).ready(function () {
        $('.modal-booking').modal('show');
    });
</script>
@endif

@endsection


