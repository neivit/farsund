@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Servicios Hotel</li>
@endsection
@section('content')

<section id="main-content">
    <div class="col-md-12">
        <a href="{{route('service.create')}}" class="btn btn-farsund">Crear servicio</a>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="bootstrap-data-table-panel">
                    <div class="table-responsive">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Nombre del documento - servicio</th>
                                    <th>Documento</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($hotelServices as $service)
                                <tr>
                                    <td>{{$service->name}}</td>
                                    <td>{{$service->file}}</td>
                                    <td><a class="btn btn-farsund btn-flat btn-addon m-b-10 m-l-5"
                                            href="{{route('service.edit', $service->id)}}" role="button"><i
                                                class="ti-settings"></i> Editar</a></td>
                                    <td>
                                        <form action="{{ route('service.destroy' , $service->id)}}" method="POST">
                                            @csrf
                                            @method("DELETE")
                                            <button type="submit" class="btn btn-farsund">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
    <!-- /# row -->
</section>

@endsection
