@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Crear Servicio - Hotel</li>
@endsection
@section('content')
<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Crear Servicio Hotel</h4>

                </div>
                <div class="card-body">
                    <div class="basic-elements">
                        <form method="post" action="{{route('service.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Extra</label>
                                <input type="text" name="name"
                                    placeholder="{{ $errors->has('name') ? $errors->first('name') : 'Nombre del archivo:' }}"
                                    class="form-control input-rounded {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                    autocomplete="off" />
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Archivo</label>
                                <input type="file" name="file" class="form-control input-rounded" />
                                <span
                                    {{ $errors->has('file') ? 'style=color:#dc3545 !important' : '' }}>{{ $errors->has('file') ? $errors->first('file') : 'Servicios - Hotel - Documento' }}</span>
                            </div>


                            <br><br>
                            <div class="row align-items-end">
                                <button type="submit" class="btn btn-farsund left">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /# row -->



</section>



<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>
@endsection
