@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Editar Servicio - Hotel</li>
@endsection
 @section('content')
 <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-title">
                                    <h4>Crear Servicio Hotel</h4>
                                    
                                </div>
                                <div class="card-body">
                                    <div class="basic-elements">
										<form method="post" action="{{route('service.update', $file->id)}}" enctype="multipart/form-data">	
										@csrf
                                        @method('PUT')
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Nombre</label>
														<input type="text" name="name" placeholder="{{ $errors->has('name') ? $errors->first('name') : 'Nombre del archivo:' }}" class="form-control input-rounded {{ $errors->has('name') ? 'is-invalid' : '' }}" autocomplete="off" value="{{$file->name}}"/>
                                                    </div>
                                                </div>
												<div class="col-lg-6">
												<div class="form-group">
												<label>Nombre: {{$file->file}}</label>
														<input type="file" name="file" class="form-control input-rounded"/>
														<span {{ $errors->has('file') ? 'style=color:#dc3545 !important' : '' }}>{{ $errors->has('file') ? $errors->first('file') : 'Servicios - Hotel - Documento' }}</span>
                                                    </div>
													</div>
                                            </div>

											<button  class="btn btn-farsund">Guardar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /# row -->


                    
                </section>



	<div class="section padding-top-bottom-smaller background-dark-2 over-hide">		
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">	
					
				</div>
			</div>
		</div>	
	</div>
@endsection