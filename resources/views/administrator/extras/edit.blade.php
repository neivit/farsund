@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Editar extra</li>
@endsection
@section('content')
<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Editar extra</h4>

                </div>
                <div class="card-body">
                    <div class="basic-elements">

                        <form method="post" action="{{route('extra.update', $extra->id)}}">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Extra</label>
                                <input type="text" class="form-control input-rounded" id="exampleFormControlInput1" name="name"
                                    value="{{$extra->name}}">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Precio</label>
                                <input type="number" class="form-control input-rounded" id="exampleFormControlInput1" name="price"
                                    value="{{$extra->price}}">
                            </div>

                            <br><br>
                            <div class="row align-items-end">
                                <button type="submit" class="btn btn-farsund left">Guardar</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /# row -->
</section>



<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>
@endsection
