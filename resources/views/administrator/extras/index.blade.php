@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Administradores</li>
@endsection
@section('content')


<section id="main-content">
    <div class="col-md-12">
        <a href="{{route('extra.create')}}" class="btn btn-farsund">Crear extra</a>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="bootstrap-data-table-panel">
                    <div class="table-responsive">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Extras</th>
                                    <th>Precio</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($extras as $extra)
                                <tr>
                                    <td>{{$extra->name}}</td>
                                    <td>{{$extra->price}}</td>
                                    <td style="text-align:center"><a
                                            class="btn btn-farsund btn-flat btn-addon m-b-10 m-l-5"
                                            href="{{route('extra.edit', $extra->id)}}" role="button"><i
                                                class="ti-settings"></i>Editar</a></td>
                                    <td style="text-align:center">
                                        <form action="{{ route('extra.destroy' , $extra->id)}}" method="POST">
                                            @csrf
                                            @method("DELETE")
                                            <button type="submit" class="btn btn-farsund">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
    <!-- /# row -->
</section>

@endsection
