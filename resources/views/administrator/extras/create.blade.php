@extends('administrator.layouts.app')

@section('module-active')
<li class="breadcrumb-item active">Crear Extra</li>
@endsection
@section('content')
<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Crear Extra</h4>

                </div>
                <div class="card-body">
                    <div class="basic-elements">

                        <form method="post" action="{{route('extra.store')}}">
                            @csrf
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Extra</label>
                                <input type="text" name="name" class="form-control input-rounded">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Precio</label>
                                <input type="number" name="price" class="form-control input-rounded">
                            </div>

                            <div class="form-group">
                                <div class="form-check">
                                    <input name="optional" class="form-check-input" type="checkbox" id="defaultCheck1">
                                    <label class="form-check-label" for="defaultCheck1">
                                        Opcional
                                    </label>
                                </div>
                            </div>

                            <br><br>
                            <div class="row align-items-end">
                                <button type="submit" class="btn btn-farsund left">Guardar</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /# row -->
</section>



<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>
@endsection
