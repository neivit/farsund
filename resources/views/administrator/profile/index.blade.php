@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Perfil</li>
@endsection
@section('content')

<div class="content-wrap">
    <div class="main">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8 p-r-0 title-margin-right">
            <div class="page-header">
              <div class="page-title">
                <h1>Hola,
                  <span>Bienvenido al panel administrativo Hotel Farsund</span>
                </h1>
              </div>
            </div>
          </div>
          <!-- /# column -->
          <div class="col-lg-4 p-l-0 title-margin-left">
            <div class="page-header">
              <div class="page-title">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="#">Dashboard</a>
                  </li>
                </ol>
              </div>
            </div>
          </div>
          <!-- /# column -->
        </div>
        <!-- /# row -->
        <section id="main-content">
          <div class="row">
            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                  <div class="user-profile">
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="user-photo m-b-30">
                          <img class="img-fluid" src="{{asset('img/profile.png')}}" alt="" />
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div class="user-profile-name">{{auth()->user()->firstname}} {{auth()->user()->lastname}}</div>
                        <div class="user-Location">
                          <i class="ti-location-pin"></i> Espana, Galicia</div>
                        <div class="user-job-title">Administrador</div>
                        <div class="custom-tab user-profile-tab">
                          <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                              <a href="{{route('profile.edit.datos')}}" aria-controls="1" >Sobre mi  <i class="fa fa-pencil-square" aria-hidden="true"></i></a> 
                            </li>
                          </ul>
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="1">
                              <div class="contact-information">
                                <h4>Informacion personal</h4>
                                <div class="phone-content">
                                  <span class="contact-title">Nombre & Apellido:</span>
                                  <span class="phone-number">{{auth()->user()->firstname}} {{auth()->user()->lastname}}</span>
                                </div>
                                <div class="phone-content">
                                  <span class="contact-title">Telefono:</span>
                                  <span class="phone-number">{{auth()->user()->phone}}</span>
                                </div>
                                <div class="email-content">
                                  <span class="contact-title">Email:</span>
                                  <span class="contact-email">{{auth()->user()->email}}</span>
                                </div>
                                <div class="email-content">
                                  <span class="contact-title">Contrasena:</span>
                                  <span class="contact-email"><a href="{{route('profile.edit.password')}}"><i class="fa fa-key" aria-hidden="true"></i> Actualizar contrasena</a></span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /# row -->
          
        
        </section>
      </div>
    </div>
  </div>

@endsection

