@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Actualizar contrasena</li>
@endsection
@section('content')
<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="basic-elements">

                        <form method="post" action="{{route('profile.update.password')}}">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Contrasena</label>
                                <input type="password" class="form-control input-rounded" id="exampleFormControlInput1" name="password">
                            </div>

                            <br><br>
                            <div class="row align-items-end">
                                <button type="submit" class="btn btn-farsund left">Guardar</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /# row -->
</section>



<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>
@endsection
