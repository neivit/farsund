@extends('administrator.layouts.app')
@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('module-active')
<li class="breadcrumb-item active">Crear Reserva</li>
@endsection
@section('content')
<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Reservar</h4>

                </div>
                <div class="card-body">
                    <div class="basic-elements">

                        <form method="post" action="{{route('booking.store')}}">
                            @csrf
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1"
                                            style="color:black;font-weight:900">Huespedes:</label>
                                        <br>
                                        <select name="user_id" class="js-example-basic-single" id="user"
                                            style="width: 300px;">
                                            <option>Buscar...</option>
                                            @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->firstname}} {{$user->lastname}} -
                                                {{$user->dni}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1"
                                            style="color:black;font-weight:900">Nuevo
                                            Huesped:</label>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Nombre</label>
                                        <input type="text" name="firstname" class="form-control input-rounded">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Apellido</label>
                                    <input type="text" name="lastname" class="form-control input-rounded">
                                </div>
                                </div>
                                <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">DNI</label>
                                    <input type="number" name="dni" class="form-control input-rounded">
                                </div>
                                </div>


                                <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Correo electronico</label>
                                    <input type="email" name="email" class="form-control input-rounded">
                                </div>
                                </div>
                                <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Telefono</label>
                                    <input type="number" name="phone" class="form-control input-rounded">
                                </div>
                                </div>

                                <br>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1"
                                            style="color:black;font-weight:900">Reservación:</label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Check In</label>
                                    <input type="date" min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")));?>"
                                        class="form-control input-rounded" name="check_in">
                                </div>
                                </div>
                                <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Check Out</label>
                                    <input type="date" min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")));?>"
                                        class="form-control input-rounded" name="check_out">
                                </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                <div class="row align-items-end">
                                    <button type="submit" class="btn btn-farsund left">Guardar</button>
                                </div>
                                </div>
                            </div>
                                
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /# row -->
</section>



<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $(document).ready(function () {
        $('.js-example-basic-single').select2();
    });

    $('#user').on('select2:select', function (e) {
        //Ajax
    });

</script>


@endsection`
