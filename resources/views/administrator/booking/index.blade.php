@extends('administrator.layouts.app')
@section('css')
<style>
    .fc-time {
        display: none;
    }
    .control-label {
        font-weight: 900;
        color: #181818;
    }
    .image-icon {
        position: relative;
        width: 50px !important;
        top: -15px !important;
    }
    .text-left {
        margin-left: 25px;
    }
</style>
@endsection
@section('module-active')
<li class="breadcrumb-item active">Reservas</li>
@endsection
@section('content')
<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <a href="{{route('booking.create')}}"  class="btn btn-lg btn-farsund btn-block waves-effect waves-light"><i class="fa fa-plus"></i> Crear reserva
                            </a>
                        </div>
                        <div class="col-md-12">
                            <div class="card-box">
                                <div id="calendar"></div>
                            </div>
                        </div>
                        <!-- end col -->
                        <!-- BEGIN MODAL -->
                        <div class="modal fade none-border" id="event-modal">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true"
                                            style="cursor: pointer;border:0;">&times;</button>
                                        <h4 class="modal-title">
                                            <strong>Reservación</strong>
                                        </h4>
                                    </div>
                                    <div class="modal-body"></div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect"
                                            data-dismiss="modal">Cerrar</button>
                                        <button type='button'
                                            class="btn btn-farsund confirm-event waves-effect waves-light">Confirmar
                                            reserva </button>
                                        <button type='button'
                                            class="btn btn-farsund finish-event waves-effect waves-light">Finalizar
                                            reserva </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
    <!-- /# row -->
</section>
@endsection

@section('js')
<script src="{{asset('administrator/js/lib/calendar/fullcalendar-init.js')}}"></script>
@endsection
