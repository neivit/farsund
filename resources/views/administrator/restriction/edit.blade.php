@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Editar Restricción</li>
@endsection
@section('content')
<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Editar Restricción</h4>

                </div>
                <div class="card-body">
                    <div class="basic-elements">

                    <form method="post" action="{{route('restriction.update', $restriction->id)}}">
                @csrf
                @method('PUT')
                <div class="form-group">
                            <label>Fecha Inicio</label>
                            <input type="text" disabled class="form-control input-rounded" value="{{$restriction->roomType->name}}">
                        </div>
                <div class="form-group">
                            <label>Fecha Inicio</label>
                            <input type="date" min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")));?>" class="form-control input-rounded" name="start" value="{{$restriction->start}}">
                        </div>
                        <div class="form-group">
                            <label>Fecha Fin</label>
                            <input type="date" min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")));?>" class="form-control input-rounded" name="end" value="{{$restriction->end}}">
                        </div>
                        <div class="form-group">
                            <label>Nombre de la restricción</label>
                            <input type="text" class="form-control input-rounded" name="name" value="{{$restriction->name}}">
                        </div>
                        <div class="form-group">
                            <label>Numero minimo de reservar</label>
                            <input type="number" class="form-control input-rounded" name="min_book" value="{{$restriction->min_book}}">
                        </div>
                        
                            <br><br>
                            <div class="row align-items-end">
                                <button type="submit" class="btn btn-farsund left">Guardar</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /# row -->
</section>



<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>
@endsection

