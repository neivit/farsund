@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Crear Restricción</li>
@endsection
@section('content')
<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Crear Restricción</h4>

                </div>
                <div class="card-body">
                    <div class="basic-elements">

                        <form action="{{route('restriction.store')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Tipo de habitación</label>
                                <select class="form-control input-rounded" name="room_type_id">
                                    <option>Seleccion el tipo de habitación</option>
                                    @foreach($roomType as $room)
                                    <option value="{{$room->id}}">{{$room->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Fecha Inicio</label>
                                <input type="date" min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")));?>"
                                    class="form-control input-rounded" name="start">
                            </div>
                            <div class="form-group">
                                <label>Fecha Fin</label>
                                <input type="date" min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")));?>"
                                    class="form-control input-rounded" name="end">
                            </div>
                            <div class="form-group">
                                <label>Nombre de la restricción</label>
                                <input type="text" class="form-control input-rounded" name="name">
                            </div>
                            <div class="form-group">
                                <label>Numero minimo de reservar</label>
                                <input type="number" class="form-control input-rounded" name="min_book">
                            </div>

                            <button type="submit" class="btn btn-farsund">Guardar</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /# row -->
</section>



<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>
@endsection

