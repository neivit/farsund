@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Restricciones</li>
@endsection
@section('content')


<section id="main-content">
    <div class="col-md-12">
        <a href="{{route('restriction.create')}}" class="btn btn-farsund ">Crear nueva restricción</a>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="bootstrap-data-table-panel">
                    <div class="table-responsive">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Inicio</th>
                                    <th>Fin</th>
                                    <th>Reserva minima</th>
                                    <th>Tipo de habitación</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($restrictions as $restriction)
                                <tr>
                                    <td>{{$restriction->name}}</td>
                                    <td>{{$restriction->start}}</td>
                                    <td>{{$restriction->end}}</td>
                                    <td>{{$restriction->min_book}}</td>
                                    <td>{{$restriction->roomType->name}}</td>
                                    <td><a class="btn btn-primary"
                                            href="{{route('restriction.edit', $restriction->id)}}" role="button"><i
                                                class="fa fa-sun-o"></i></a></td>
                                    <td>
                                        <form action="{{ route('restriction.destroy' , $restriction->id)}}"
                                            method="POST">
                                            @csrf
                                            @method("DELETE")
                                            <button type="submit" class="btn btn-farsund">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
    <!-- /# row -->
</section>

@endsection
