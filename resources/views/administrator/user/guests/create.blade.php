@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Registrar Huesped</li>
@endsection
@section('content')
<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Registrar Huesped</h4>

                </div>
                <div class="card-body">
                    <div class="basic-elements">
                        <form method="post" action="{{route('guest.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Nombre</label>
                                        <input type="text" name="firstname" class="form-control input-rounded">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Apellido</label>
                                        <input type="text" name="lastname" class="form-control input-rounded">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">DNI</label>
                                        <input type="number" name="dni" class="form-control input-rounded">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Correo electronico</label>
                                        <input type="email" name="email" class="form-control input-rounded">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Contraseña</label>
                                        <input type="password" name="password" class="form-control input-rounded">
                                    </div>
                                </div>
                            </div>
                    </div>
                    <button class="btn btn-farsund">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    </div>
    <!-- /# row -->



</section>



<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>
@endsection
