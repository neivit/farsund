@extends('administrator.layouts.app')
@section('css')
<style>
    .action {
        padding: 0 10px !important;
    }

    .image-icon {
        position: relative;
        width: 80px !important;
        top: -15px !important;
    }

    .stat-heading {
        font-size: 30px !important;
        font-weight: bold !important;
        position: relative;
        left: 25px;
        top: 15px;
        font-weight: bold;
        color: #fff;
    }

    .stat-text {
        font-size: 15px !important;
        text-align: center;
        font-weight: bold;
        color: #fff;
    }

    .status {
        text-align: center;
        font-weight: bold;
        color: #fff;
    }

</style>
@endsection
@section('module-active')
<li class="breadcrumb-item active">Administradores</li>
@endsection
@section('content')


<section id="main-content">
    <div class="col-md-12">
        <a href="{{route('room.create')}}" class="btn btn-farsund">Crear nueva
            habitación</a>
    </div>


    <!-- Room availability -->
    <div class="row">
        @foreach($rooms as $room)
        <div class="col-lg-3">
            <div class="card bg-room room-{{$room->id}}"
                style="background: {{ $room->status_id == 4 ? '#28a745' : '#dc3545' }}">
                <label class="status">
                    @if($room->status_id == 4)
                    <span class="ti-check form-control-feedback"></span> Disponible
                    @else
                    <span class="ti-alert form-control-feedback"></span> Ocupada
                    @endif
                </label>
                <div class="stat-widget-six">
                    <div class="stat-icon">
                        <img src="{{asset('img/room-hotel.png')}}" class="image-icon">
                    </div>
                    <div class="stat-content">
                        <div class="text-left dib">
                            <div class="stat-heading">{{$room->room_number}}</div>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="stat-text">{{$room->roomType->name}}</div>
                <br>
                <div class="stat-text">
                    
                <button type="button" class="btn btn-farsund">Ocupar</button>
                  
                </div>
                <!-- <div class="stat-text">
                    <form action="{{ route('room.destroy' , $room->id)}}" method="POST">
                        @csrf
                        @method("DELETE")
                        <button type="submit" class="btn btn-farsund"><i class="fa fa-trash"
                                aria-hidden="true"></i></button>
                    </form>
                </div> -->
            </div>
        </div>
        @endforeach
    </div>
    <!-- Room availability -->



    <!-- /# row -->
</section>

@endsection


@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#refundable').on('change', function () {
            $('#refundable_not').not(this).prop('checked', false);
        });
        $('#refundable_not').on('change', function () {
            $('#refundable').not(this).prop('checked', false);
        });
    });

</script>
@endsection
