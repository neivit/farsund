@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Crear Habitación</li>
@endsection
@section('content')
<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Crear Habitación</h4>

                </div>
                <div class="card-body">
                    <div class="basic-elements">

                    <form method="post" action="{{route('room.store')}}">
                        @csrf
                        
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Tipo de habitación</label>
                            <select name="room_type_id" class="form-control input-rounded" id="exampleFormControlSelect1" name="adult">
                                <option value="0">Seleccione</option>
                                @foreach($roomTypes as $room)
                                 <option value="{{$room->id}}">{{$room->name}}</option>
                                @endforeach
                                
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Numero de habitación</label>
                            <input type="number" name="room_number" class="form-control input-rounded">
                        </div>
                        
                        <br><br>
                        <div class="row align-items-end">
                            <button type="submit" class="btn btn-farsund left">Guardar</button>
                        </div>
                    </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /# row -->
</section>



<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>
@endsection
