@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Crear Tipo Habitación</li>
@endsection
@section('content')
<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Crear Tipo Habitación</h4>
                </div>
                <div class="card-body">
                    <div class="basic-elements">
                        <form method="post" action="{{route('roomtype.store')}}">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Tipo de habitación</label>
                                        <input type="text" name="name" class="form-control input-rounded">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Adultos</label>
                                        <select name="adult" class="form-control input-rounded"
                                            id="exampleFormControlSelect1">
                                            <option>Seleccione</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Niños</label>
                                        <select name="children" class="form-control input-rounded"
                                            id="exampleFormControlSelect1">
                                            <option>Seleccione</option>
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Descripción</label>
                                <textarea name="description" class="form-control " id="exampleFormControlTextarea1"
                                    rows="4" style="height: auto;" name="description"></textarea>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Precio</label>
                                        <input type="number" name="price" class="form-control input-rounded">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Reembolsable</label>
                                        <select name="refundable" class="form-control input-rounded"
                                            id="exampleFormControlSelect1">
                                            <option>Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>

                                        </select>
                                    </div>
                                    @error('refundable')
                                    <p class="error-message">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Numero de dias para reembolsal</label>
                                        <input type="number" name="refundable_num" class="form-control input-rounded">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                @foreach($extras as $extra)
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <input type="checkbox" class="form-check-input" name="extra_id[]"
                                            value="{{$extra->id}}">
                                        <label class="form-check-label" for="exampleCheck1"
                                            style="margin-right: 40px;">{{$extra->name}}
                                            {{$extra->price}} € {{$extra->optional ? '(Opcional)' : ''}}
                                        </label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <br><br>
                            <div class="row align-items-end">
                                <button type="submit" class="btn btn-farsund left">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /# row -->
</section>



<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>

<!-- <script>
    window.onload = function () {
        var test = document.querySelectorAll('.optional')
        for (var i = 0; i < test.length; i++) {
            test[i].addEventListener("click", function () {
                
                
                if (document.getElementById(this.id).checked == true) {
                    document.getElementById(this.id).value = 1;
                }

            });
        }
    }

</script> -->

@endsection
