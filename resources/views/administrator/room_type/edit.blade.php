@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Crear Tipo Habitación</li>
@endsection
@section('content')
<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Crear Tipo Habitación</h4>

                </div>
                <div class="card-body">
                    <div class="basic-elements">

                        <form method="post" action="{{route('roomtype.update', $roomType->id)}}">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Tipo de habitación</label>
                                        <input type="text" name="name" class="form-control input-rounded"
                                            value="{{$roomType->name}}">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Adultos</label>
                                        <select name="adult" class="form-control input-rounded"
                                            id="exampleFormControlSelect1">
                                            <option>Seleccione</option>
                                            <option value="1" {{ $roomType->adult == 1 ? 'selected' : '' }}>1</option>
                                            <option value="2" {{ $roomType->adult == 2 ? 'selected' : '' }}>2</option>
                                            <option value="3" {{ $roomType->adult == 3 ? 'selected' : '' }}>3</option>
                                            <option value="4" {{ $roomType->adult == 4 ? 'selected' : '' }}>4</option>
                                            <option value="5" {{ $roomType->adult == 5 ? 'selected' : '' }}>5</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Niños</label>
                                        <select name="children" class="form-control input-rounded"
                                            id="exampleFormControlSelect1">
                                            <option>Seleccione</option>
                                            <option value="0" {{ $roomType->children == 0 ? 'selected' : '' }}>0
                                            </option>
                                            <option value="1" {{ $roomType->children == 1 ? 'selected' : '' }}>1
                                            </option>
                                            <option value="2" {{ $roomType->children == 2 ? 'selected' : '' }}>2
                                            </option>
                                            <option value="3" {{ $roomType->children == 3 ? 'selected' : '' }}>3
                                            </option>
                                            <option value="4" {{ $roomType->children == 4 ? 'selected' : '' }}>4
                                            </option>
                                            <option value="5" {{ $roomType->children == 5 ? 'selected' : '' }}>5
                                            </option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Descripción</label>
                                <textarea name="description" class="form-control " id="exampleFormControlTextarea1"
                                    rows="4" style="height: auto;"
                                    name="description">{{$roomType->description}}</textarea>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Precio</label>
                                        <input type="number" name="price" class="form-control input-rounded"
                                            value="{{$roomType->price}}">
                                    </div>
                                </div>
                            </div>
                            @if($roomType->refundable)
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Reembolsable</label>
                                        <select name="refundable" class="form-control input-rounded"
                                            id="exampleFormControlSelect1">
                                            <option>Seleccione</option>
                                            <option value="1" {{ $roomType->refundable == 1 ? 'selected' : '' }}>Si
                                            <option value="0" {{ $roomType->refundable == 0 ? 'selected' : '' }}>No
                                        </select>
                                    </div>
                                    @error('refundable')
                                    <p class="error-message">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Numero de dias para reembolsal</label>
                                        <input type="number" name="refundable_num" class="form-control input-rounded"
                                            value="{{$roomType->refundable_num}}">
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                @foreach($extras as $extra)
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <input type="checkbox" class="form-check-input extras" name="extra_id[]"
                                            value="{{$extra->id}}" {{ $extra->id == $extra->extra_id ? 'checked' : '' }}>
                                        <label class="form-check-label" for="exampleCheck1"
                                            style="margin-right: 40px;">{{$extra->name}}
                                            {{$extra->price}} € {{$extra->optional ? '(Opcional)' : ''}}
                                        </label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <br><br>
                            <div class="row align-items-end">
                                <button type="submit" class="btn btn-farsund left">Guardar</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /# row -->
</section>



<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>

@endsection
