@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Administradores</li>
@endsection
@section('content')


<section id="main-content">
    <div class="col-lg-3">
        <a href="{{route('roomtype.create')}}" class="btn btn-farsund">Crear tipo de
            habitación </a>
    </div>


    <!-- Room Types -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="bootstrap-data-table-panel">
                    <div class="table-responsive">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Tipo de habitación</th>
                                    <th>Descripción</th>
                                    <th>Adultos</th>
                                    <th>Niños</th>
                                    <th>Precio</th>
                                    <th>Reembolsable</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($roomTypes as $room)
                                <tr>
                                    <td width="150">{{$room->name}}</td>
                                    <td width="300">{{$room->description}}</td>
                                    <td width="70">{{$room->adult}}</td>
                                    <td width="70">{{$room->children}}</td>
                                    <td width="100">{{$room->price}}€</td>
                                    <td width="100">
                                    @if($room->refundable)
                                        {{$room->refundable_num}} Dias
                                    @else
                                       No aplica
                                    @endif
                                    </td>
                                    <td style="text-align:center"><a
                                            class="btn btn-farsund btn-flat btn-addon m-b-10 m-l-5"
                                            href="{{route('roomtype.edit', $room->id)}}" role="button"><i
                                                class="ti-settings"></i>Editar</a></td>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
    <!-- /# row -->
</section>

@endsection
