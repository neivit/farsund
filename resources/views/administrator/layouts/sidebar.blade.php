<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
      <div class="nano">
        <div class="nano-content">
          <div class="logo"><a href="#"><!-- <img src="assets/images/logo.png" alt="" /> --><span>Hotel Farsund</span></a></div>
          <ul>
            <li class="label">Administración</li>
            <li><a href="{{route('profile.index')}}"><i class="ti-user"></i> Dashboard</a></li>
            <li><a href="{{route('booking.index')}}"><i class="ti-calendar"></i> Rerservas </a></li>
            <li><a href="{{route('roomtype.index')}}"><i class="ti-home"></i> Tipo de Habitaciones </a></li>
            <li><a href="{{route('room.index')}}"><i class="ti-home"></i> Habitaciones </a></li>
            <li><a href="{{route('guest.index')}}"><i class="ti-user"></i> Huéspedes </a></li>
            <li><a href="{{route('administrator.index')}}"><i class="ti-user"></i> Administradores</a></li>
            <li><a href="{{route('restriction.index')}}"><i class="ti-home"></i> Restricciones</a></li>
            <li><a href="{{route('extra.index')}}"><i class="ti-home"></i> Extras - Habitación</a></li>
            <li><a href="{{route('order.index')}}"><i class="ti-home"></i> Ordenes</a></li>
            <li><a href="{{route('service.index')}}"><i class="ti-view-list-alt"></i> Servicios Hotel </a></li>
            <li><a href="{{route('home')}}"><i class="ti-world"></i> Hotel Farsund</a></li> 
            <li><a href="{{route('logout.user')}}"><i class="ti-power-off"></i> Cerrar Sesión</a></li>
          </ul>
        </div>
      </div>
    </div>