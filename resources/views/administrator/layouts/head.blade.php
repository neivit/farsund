	<!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">
	
	<link href="{{ asset('administrator/css/lib/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('administrator/css/lib/themify-icons.css') }}" rel="stylesheet">
	<link href="{{ asset('administrator/css/lib/menubar/sidebar.css') }}" rel="stylesheet">
	<link href="{{ asset('administrator/css/lib/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('administrator/css/lib/helper.css') }}" rel="stylesheet">	
	<link href="{{ asset('administrator/css/style.css') }}" rel="stylesheet">
    @if(in_array(Route::currentRouteName(),
    [
        'room.index',
        'booking.index'
    
    ]))
    <!-- Toastr -->
    <link href="{{ asset('administrator/css/lib/toastr/toastr.min.css') }}" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="{{ asset('administrator/css/lib/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <!-- JsGrid -->
    <link href="{{ asset('administrator/css/lib/jsgrid/jsgrid-theme.min.css') }}" rel="stylesheet">
    <link href="{{ asset('administrator/css/lib/jsgrid/jsgrid.min.css') }}" rel="stylesheet">
    <!-- Datatable -->
    <link href="{{ asset('administrator/css/lib/datatable/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('administrator/css/lib/data-table/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="{{ asset('administrator/css/lib/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('administrator/css/lib/owl.theme.default.min.css') }}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset('administrator/css/lib/select2/select2.min.css') }}" rel="stylesheet">
    <!-- Calender -->
    <link href="{{ asset('administrator/css/lib/calendar/fullcalendar.css') }}" rel="stylesheet">
    <style>
        .fc-title{
            font-weight: bold;
            color:#fff;
        }
        .fc-time{
            /* display: none; */
            color:red;
        }
    </style>
    @endif