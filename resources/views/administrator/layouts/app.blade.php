<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if(in_array(Route::currentRouteName(),
    ['booking.index']))
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @endif
    <title>Farsund</title>

    @include('administrator.layouts.head')
    @yield('css')
</head>

<body>
    <!-- sidebar -->
    @include('administrator.layouts.sidebar')
    <!-- header -->
    @include('administrator.layouts.header')

    
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right"></div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                    @yield('module-active')
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->

				<!-- Content ===== -->
				@yield('content')
            </div>
        </div>
    </div>

    @include('administrator.layouts.footer')
    @yield('js')
</body>

</html>
