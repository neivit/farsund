    <script src="{{ asset('administrator/js/lib/jquery.min.js') }}"></script>
	<script src="{{ asset('administrator/js/lib/jquery.nanoscroller.min.js') }}"></script>
	<script src="{{ asset('administrator/js/lib/menubar/sidebar.js') }}"></script>
	<script src="{{asset('administrator/js/lib/bootstrap.min.js')}}"></script>  
	<script src="{{asset('administrator/js/scripts.js')}}"></script> 
	@if(in_array(Route::currentRouteName(),
    [
        'room.index',
        'booking.index'
    ]))
	<script src="{{asset('administrator/js/lib/jquery-ui/jquery-ui.min.js')}}"></script>
  	<script src="{{asset('administrator/js/lib/moment/moment.js')}}"></script>
  	<script src="{{asset('administrator/js/lib/calendar/fullcalendar.js')}}"></script>
  	<script src="{{asset('administrator/js/lib/calendar/es.js')}}"></script>
  	<script src="{{asset('administrator/js/toastr.min.js')}}"></script>
  	<script src="{{asset('administrator/js/toastr.init.js')}}"></script>
	@endif
