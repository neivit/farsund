@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Ordenes de pago</li>
@endsection
@section('content')


<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="bootstrap-data-table-panel">
                    <div class="table-responsive">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Num.Orden</th>
                                    <th>Huesped</th>
                                    <th>Reservacion</th>
                                    <th>Precio</th>
                                    <th>Iva</th>
                                    <th>Total</th>
                                    <th>Fecha</th>
                                    <th>Editar</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <td>{{$order->order_number}}</td>
                                    <td>{{$order->booking->user->firstname}} {{$order->booking->user->lastname}}</td>
                                    <th>{{$order->booking->room->roomType->name}}</th>
                                    <th>{{$order->price}}€</th>
                                    <th>{{$order->iva}}</th>
                                    <th>{{$order->total}}€</th>
                                    <th>{{\Carbon\Carbon::parse($order->date)->translatedFormat('j \d\e F \d\e\l Y  h:i:s A')}}</th>
                                    <td><a class="btn btn-farsund" href="{{route('order.edit', $order->id)}}"
                                            role="button">Ver </a></td>
                                    <td>
                                    <form action="{{ route('order.destroy' , $order->id)}}" method="POST">
                                            @csrf
                                            @method("DELETE")
                                            <button type="submit" class="btn btn-farsund">Eliminar</button>
                                        </form>
                                    </td>
                                   
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
    <!-- /# row -->
</section>

@endsection
