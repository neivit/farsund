@extends('administrator.layouts.app')
@section('module-active')
<li class="breadcrumb-item active">Editar Orden</li>
@endsection
@section('content')
<section id="main-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Editar Orden</h4>

                </div>
                <div class="card-body">
                    <div class="basic-elements">                
                <div class="form-group">
                            <label>Num.Orden</label>
                            <input class="form-control input-rounded" type="text" value="{{$order->order_number}}" readonly="">
                        </div>
                <div class="form-group">
                            <label>Huesped</label>
                            <input class="form-control input-rounded" type="text" value="{{$order->booking->user->firstname}} {{$order->booking->user->lastname}}" readonly="">
                        </div>
                        <div class="form-group">
                            <label>Reservacion</label>
                            <input class="form-control input-rounded" type="text" value="{{$order->booking->room->roomType->name}}" readonly="">
                        </div>
                        <div class="form-group">
                            <label>Precio €</label>
                            <input class="form-control input-rounded" type="number" value="{{$order->price}}" readonly="">
                        </div>
                        <div class="form-group">
                            <label>Iva</label>
                            <input  class="form-control input-rounded" type="number" value="{{$order->iva}}" readonly="">
                        </div>
                        <div class="form-group">
                            <label>Total €</label>
                            <input class="form-control input-rounded" type="number" value="{{$order->total}}" readonly="">
                        </div>
                        <div class="form-group">
                            <label>Fecha</label>
                            <input type="date" min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")));?>" class="form-control input-rounded" readonly="" value="<?php echo date('Y-m-d', strtotime($order->date)) ?>">
                        </div>
                    </div>
                    <a href="{{route('order.index')}}" class="btn btn-farsund left">Regresar</a>
                </div>
            </div>
        </div>

    </div>
    <!-- /# row -->
</section>



<div class="section padding-top-bottom-smaller background-dark-2 over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">
    $(document).ready(function (){
        $('#refundable').on('change', function () {
            $('#refundable_not').not(this).prop('checked', false);
        });
        $('#refundable_not').on('change', function () {
            $('#refundable').not(this).prop('checked', false);
        });
    });

    $('#multiselect').multiselect({
        nonSelectedText: 'Extras',
        templates: {
            li: '<li><a href="javascript:void(0);"><label class="pl-2"></label></a></li>'
        }
    });
</script>
@endsection
