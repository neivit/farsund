@extends('layouts.app')
 @section('content')
	<div class="section big-55-height over-hide z-bigger">
	
		<div class="parallax parallax-top" style="background-image: url('img/header.jpg')"></div>
		<div class="dark-over-pages"></div>
	
		<div class="hero-center-section pages">
			<div class="container">
				<div class="row justify-content-center">
				</div>
			</div>
		</div>
	</div>
	
	<div class="section padding-top-bottom-smaller background-dark over-hide z-too-big" style="padding-top:50px">
		<div class="section">		
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="row justify-content-center">
                            <div class="hero-text">{{trans('messages.lugar_interes')}}</div>
						</div>	
					</div>
				</div>
			</div>					
		</div>
	</div>
	
	<div class="section padding-top-bottom background-grey over-hide">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8 align-self-center">
					<div class="subtitle with-line text-center mb-4">Hotel</div>
					<h3 class="text-center padding-bottom-small">Farsund</h3>
				</div>
				<div class="section clearfix"></div>
			</div>
			<div class="row background-white p-0 m-0">
				<div class="col-xl-6 p-0">
					<div class="img-wrap" id="rev-1">
						<img src="img/isla_cien_ons.jpg" alt="">
					</div>
				</div>
				<div class="col-xl-6 p-0 align-self-center">
					<div class="row justify-content-center">
						<div class="col-9 pt-4 pt-xl-0 pb-5 pb-xl-0 text-center">
							<h5 class="" style="text-align:center;margin-left:16px;">{{trans('messages.isla_cien_ons')}}</h5>
							<p class="mt-3">{{trans('messages.cien_ons')}}</p>
						</div>
					</div>	
				</div>
			</div>
			<div class="row background-white p-0 m-0">
				<div class="col-xl-6 p-0 align-self-center">
					<div class="row justify-content-center">
						<div class="col-9 pt-4 pt-xl-0 pb-5 pb-xl-0 text-center">
							<h5 class="" style="text-align:center;">{{trans('messages.costa_muerte')}}</h5>
							<p class="mt-3">{{trans('messages.costa_muerte_2')}}</p>
						</div>
					</div>
				</div>
				<div class="col-xl-6 order-first order-xl-last p-0">
					<div class="img-wrap" id="rev-2">
						<img src="img/costa_muerte.jpg" alt="">
					</div>
				</div>
			</div>
			<div class="row background-white p-0 m-0">
				<div class="col-xl-6 p-0">
					<div class="img-wrap" id="rev-3">
						<img src="img/catedral_santiago.jpg" alt="">
					</div>
				</div>
				<div class="col-xl-6 p-0 align-self-center">
					<div class="row justify-content-center">
						<div class="col-9 pt-4 pt-xl-0 pb-5 pb-xl-0 text-center">
							<h5 class="">{{trans('messages.catedral_santiago')}}</h5>
							<p class="mt-3">{{trans('messages.cate_santia')}}</p>
						</div>
					</div>	
				</div>
			</div>
			<div class="row background-white p-0 m-0">
				<div class="col-xl-6 p-0 align-self-center">
					<div class="row justify-content-center">
						<div class="col-9 pt-4 pt-xl-0 pb-5 pb-xl-0 text-center">
							<h5 class="">{{trans('messages.combarro')}}</h5>
							<p class="mt-3">{{trans('messages.comba')}}</p>
						</div>
					</div>
				</div>
				<div class="col-xl-6 order-first order-xl-last p-0">
					<div class="img-wrap" id="rev-4">
						<img src="img/combarro.jpg" alt="">
					</div>
				</div>
			</div>
			<div class="row background-white p-0 m-0">
				<div class="col-xl-6 p-0">
					<div class="img-wrap" id="rev-2">
						<img src="img/playa_catedral.jpg" alt="">
					</div>
				</div>
				<div class="col-xl-6 p-0 align-self-center">
					<div class="row justify-content-center">
						<div class="col-9 pt-4 pt-xl-0 pb-5 pb-xl-0 text-center">
							<h5 class="">{{trans('messages.playa_catedrales')}}</h5>
							<p class="mt-3">{{trans('messages.playa_catedral')}}</p>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
	
	
@endsection