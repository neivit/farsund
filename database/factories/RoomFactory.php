<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Status;
use App\Models\RoomType;
use App\Models\Room;

class RoomFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Room::class;

    public function definition()
    {
        return [
            'room_number'  =>   $this->faker->unique()->numberBetween(1, 100),
            'room_type_id' => random_int(1,2),
            'status_id'    => 4
        ];
    }
}
