<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('room_types')->insert([
            'name'  => 'Estandar',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
            'adult'  => random_int(1,5),
            'children'  => random_int(0,5),
            'price' => random_int(50,200),
            'refundable' => false,
            'refundable_num' => null,
        ]);
        DB::table('room_types')->insert([
            'name'        => 'Superior',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
            'adult'  => random_int(1,5),
            'children'  => random_int(0,5),
            'price' => random_int(50,200),
            'refundable' => false,
            'refundable_num' => null,
        ]);
    }
}
