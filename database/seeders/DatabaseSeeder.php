<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Room;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {   

        $this->call(StatusSeeder::class);
        //$this->call(RoomTypeSeeder::class);
        //$this->call(ExtraSeeder::class);
        //Room::factory(15)->create();
        //$this->call(RoomExtraSeeder::class);
        DB::table('users')->insert([
            'firstname'    => 'Andres',
            'dni'          => '123456789',
            'email'        => 'info@hotelfarsund.es',
            'phone'        => '12345678910',
            'password'     => Hash::make('admin123'),
            'admin'        => '1',
            'email_verified_at' => now(),
            'created_at'  => now(),
            'updated_at'  => now(),
            'remember_token' => Str::random(10),
        ]);     
    }
}
