<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExtraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('extras')->insert([
            'name'   => 'Desayuno',
            'price'  => random_int(5,20),
        ]);
        DB::table('extras')->insert([
            'name'   => 'Cuna bebe',
            'price'  => random_int(5,20),
        ]);
        DB::table('extras')->insert([
            'name'   => 'Cama extra',
            'price'  => random_int(5,20),
        ]);
    }
}
