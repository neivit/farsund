<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            'name'   => 'Pendiente',
            'state'  => '#f4df4e'
        ]);
        DB::table('statuses')->insert([
            'name'   => 'Ocupada',          
            'state'  => '#dc3545'
        ]);
        DB::table('statuses')->insert([
            'name'   => 'Reserva finalizada',
            'state'  => '#00bfff'
        ]);
        DB::table('statuses')->insert([
            'name'   => 'Disponible',          
            'state'  => '#28a745'
        ]);
    }
}
