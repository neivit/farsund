<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Dashboard\ProfileController;
use App\Http\Controllers\Dashboard\BookingController;
use App\Http\Controllers\Dashboard\RoomTypeController;
use App\Http\Controllers\Dashboard\RoomController;
use App\Http\Controllers\Dashboard\GuestController;
use App\Http\Controllers\Dashboard\AdministratorController;
use App\Http\Controllers\Dashboard\RestrictionController;
use App\Http\Controllers\Dashboard\ExtraController;
use App\Http\Controllers\Dashboard\OrderController;
use App\Http\Controllers\Dashboard\ServiceController;

use App\Http\Controllers\Portal\HotelFarsundController;
use App\Http\Controllers\Portal\BookingPaymentController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\LenguageController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HotelFarsundController::class, 'home'])->name('home');
Route::get('sobre-nosotros', [HotelFarsundController::class, 'about'])->name('about');
Route::get('habitaciones', [HotelFarsundController::class, 'rooms'])->name('rooms');
Route::get('reservar/{id}/{chechin?}/{checkout?}', [HotelFarsundController::class, 'book'])->name('book');
Route::get('servicios', [HotelFarsundController::class, 'services'])->name('services');
Route::get('lugares-de-interes', [HotelFarsundController::class, 'placeInterest'])->name('places_interest');
Route::get('contacto', [HotelFarsundController::class, 'contact'])->name('contact');
Route::post('contacto/enviar', [HotelFarsundController::class, 'send'])->name('send.contact');
Route::get('policy-privacy', [HotelFarsundController::class, 'policyPrivacy'])->name('policy.privacy');
Route::get('terms-conditions', [HotelFarsundController::class, 'termsConditions'])->name('terms.conditions');


Route::middleware(['guest'])->group(function () {
    /* User - Farsund */    
    Route::get('sig-up', [UserController::class, 'create'])->name('create.user');
    Route::post('sig-up/store', [UserController::class, 'store'])->name('store.user');
    Route::get('sig-in', [UserController::class, 'login'])->name('login');
    Route::post('sig-in/authentication', [UserController::class, 'authentication'])->name('authentication');
});

Route::middleware('auth')->group(function(){
    /* User - Farsund */    
    Route::get('logout', [UserController::class, 'logout'])->name('logout.user');
    Route::post('reservacion/{room}', [BookingPaymentController::class, 'store'])->name('payment');
    Route::get('confirm', [BookingPaymentController::class, 'show'])->name('confirm');
    Route::get('approbal', [BookingPaymentController::class, 'approbal'])->name('approbal');

});



Route::group(['middleware' => 'admin', 'prefix' => 'dashboard'], function(){
    
    Route::resource('/profile', ProfileController::class);
    Route::get('/profile/edit/datos', [ProfileController::class, 'editProfile'])->name('profile.edit.datos');
    Route::PUT('/profile/update/datos', [ProfileController::class, 'updateProfile'])->name('profile.update.datos');
    Route::get('/profile/edit/password', [ProfileController::class, 'editPass'])->name('profile.edit.password');
    Route::PUT('/profile/update/password', [ProfileController::class, 'updatePass'])->name('profile.update.password');
    Route::resource('/booking', BookingController::class);
    Route::post('/booking/confirm', [BookingController::class, 'confirm'])->name('booking.confirm');
    Route::post('/booking/finish', [BookingController::class, 'finish'])->name('booking.finish');
    Route::resource('/roomtype', RoomTypeController::class);
    Route::resource('/room', RoomController::class);
    Route::resource('/guest', GuestController::class);
    Route::resource('/administrator', AdministratorController::class);
    Route::resource('/restriction', RestrictionController::class);
    Route::resource('/extra', ExtraController::class);
    Route::resource('/order', OrderController::class);
    Route::resource('/service', ServiceController::class);
    
});

/*Lenguage*/
Route::get('lang/{lang}', [LenguageController::class, 'swap'])->name('lang.swap');

