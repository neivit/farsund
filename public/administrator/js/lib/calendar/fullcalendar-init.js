
!function($) {
    "use strict";

    var CalendarApp = function() {
        this.$body = $("body")
        this.$modal = $('#event-modal'),
        this.$event = ('#external-events div.external-event'),
        this.$calendar = $('#calendar'),
        this.$saveRoomBtn = $('.save-room'),
        this.$extEvents = $('#external-events'),
        this.$calendarObj = null
    };

    
    /* on click on event */
    /* Para actualizar y eliminar evento */
    CalendarApp.prototype.onEventClick =  function (calEvent, jsEvent, view) {
            var $this = this;
            $this.$modal.modal({ backdrop: 'static' });
            let dateEnd = moment(calEvent.end).format('YYYY-MM-DD').valueOf();
            let start = moment(calEvent.start).format('YYYY-MM-DD')
            let end = moment(calEvent.end).format('YYYY-MM-DD')

            // if( dateEnd == "Fecha invalida"){
            //     var start = moment(calEvent.start).format('YYYY-MM-DD');
            //     var end = moment(calEvent.start).format('YYYY-MM-DD');
            // }
            // else{
            //     var start = moment(calEvent.start).format('YYYY-MM-DD');
            //     var end = moment(calEvent.end).format('YYYY-MM-DD');
            // }
           

            var form = $("<form id='form-edit'></form>");
            form.append("<input id='booking-id' class='form-control' type=hidden value='" + calEvent.booking_id + "'/><div class='row'><div class='col-md-6'><label class='control-label'>Tipo de Habitación</label>  <p class='form-control-static'>"+ calEvent.name +"</p></div><div class='col-md-6'><label class='control-label'>Estatus</label><p class='form-control-static'>"+ calEvent.status +"</p></div><div class='col-md-6'><label class='control-label'>Huesped</label><p class='form-control-static'>"+ calEvent.huesped +"</p></div><div class='col-md-6'><label class='control-label'>Check In</label><input id='start-edit' class='form-control' type=date  value='" + start + "'/></div><div class='col-md-6'><label class='control-label'>Check Out</label><input id='end-edit' class='form-control' type=date  value='" + end + "'/></div></div>");
            

            $this.$modal.find('.finish-event').show().end().find('.modal-body').empty().prepend(form).end().find('.finish-event').unbind('click').on("click", function (e) {
                    e.preventDefault();
                    
                    $.ajax({
                        type: "post",
                        url: "/dashboard/booking/finish",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{
                            booking: $('#booking-id').val(),
                            
                        },
                        success:function(response){
                            if($.isEmptyObject(response.error)){
                                $this.$modal.modal('hide');
                                $('#calendar').fullCalendar('refetchEvents');
                                $('#form-edit')[0].reset();
                                toastr.success('Reserva finalizada',{timeOut:3000})
                            }
                            else{
                                printErrorMsg(response.error);
                            }
                        }
                    });
    
                    function printErrorMsg (msg) {
                        $.each( msg, function( key, value ) {
                        console.log(key);
                          $('.'+key+'_err').text(value);
                        });
                    }
    
                    $this.$calendarObj.fullCalendar('removeEvents', function (ev) {
                        return (ev._id == calEvent._id);
                    });
               
            });


            $this.$modal.find('.confirm-event').show().end().find('.modal-body').empty().prepend(form).end().find('.confirm-event').unbind('click').on("click", function (e) {
                e.preventDefault();
                
                $.ajax({
                    type: "post",
                    url: "/dashboard/booking/confirm",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data:{
                        booking: $('#booking-id').val(),
                    },
                    success:function(response){
                        if($.isEmptyObject(response.error)){
                            $this.$modal.modal('hide');
                            $('#calendar').fullCalendar('refetchEvents');
                            $('#form-edit')[0].reset();
                            toastr.success('Reserva confirmada',{timeOut:3000})
                        }
                        else{
                            printErrorMsg(response.error);
                        }
                    }
                });

                function printErrorMsg (msg) {
                    $.each( msg, function( key, value ) {
                    console.log(key);
                      $('.'+key+'_err').text(value);
                    });
                }

                $this.$calendarObj.fullCalendar('removeEvents', function (ev) {
                    return (ev._id == calEvent._id);
                });
           
        });
        
    },


    CalendarApp.prototype.enableDrag = function() {
        //init events
        $(this.$event).each(function () {
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };
            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });
        });
    }
    /* Initializing */
    CalendarApp.prototype.init = function() {
        this.enableDrag();
        /*  Initialize the calendar  */
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var form = '';
        var today = new Date($.now());

        var $this = this;
        $this.$calendarObj = $this.$calendar.fullCalendar({
            slotDuration: '00:15:00', /* If we want to split day time each 15minutes */
            minTime: '08:00:00',
            maxTime: '19:00:00', 
            defaultView: 'month',  
            handleWindowResize: true,   
            height: $(window).height() - 200,   
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month'
            },
            events: '/dashboard/booking',
            locale: 'es',
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            selectable: true,
            /* move update */
           
            eventClick: function(calEvent, jsEvent, view) { $this.onEventClick(calEvent, jsEvent, view); 
                //Event Modal Calendar - Abre modal al dar clic en el Item del calendar
            }
            
        });

       
    },

   //init CalendarApp
    $.CalendarApp = new CalendarApp, $.CalendarApp.Constructor = CalendarApp
    
}(window.jQuery),

//initializing CalendarApp
function($) {
    "use strict";
    $.CalendarApp.init()
}(window.jQuery);
