<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\OrderTrait;
use Carbon\Carbon;

class Order extends Model
{
    use HasFactory;
    use OrderTrait;

    protected $fillable = ['order_number','date','price','iva','total','booking_id'];

    
    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }
}
