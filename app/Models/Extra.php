<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Extra extends Model
{
    use HasFactory;

    protected $fillable = ['name','price','optional'];

    public function setNameAttribute($value)
    {
      $this->attributes['name'] = strtolower($value);
    }

    public function bookings()
    {
        return $this->belongsToMany(Booking::class);
    }

    public function roomTypes()
    {
        return $this->belongsToMany(RoomType::class);
    }
    

}
