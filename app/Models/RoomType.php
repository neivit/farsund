<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    use HasFactory;

    protected $fillable = ['name','description','adult','children','price','refundable','refundable_num'];
    

    public function setNameAttribute($value)
    {
      $this->attributes['name'] = strtolower($value);
    }
    
    
    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function extras()
    {
        return $this->belongsToMany(Extra::class);
    }
    

    public function restrictions()
    {
        return $this->hasMany(Restriction::class);
    }
}
