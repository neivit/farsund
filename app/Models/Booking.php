<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\BookingTrait;

class Booking extends Model
{
    use HasFactory;
    use BookingTrait;

    protected $fillable = ['check_in','check_out','user_id','room_type_id','status_id'];   
    

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function roomType()
    {
        return $this->belongsTo(Room::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function extras()
    {
        return $this->belongsToMany(Extra::class);
    }




   
}
