<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restriction extends Model
{
    use HasFactory;

    protected $fillable = ['name','start','end','min_book','room_type_id'];

    public function roomType()
    {
        return $this->belongsTo(RoomType::class);
    }
}
