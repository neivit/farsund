<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\User\SigInRequest;
use App\Http\Requests\User\SigUpRequest;
use Auth;

class UserController extends Controller
{
    public function create(){
        return view('auth.sign-up');
    }

    public function store(SigUpRequest $request){
        $user = new User($request->all());
        $user->save();
        
        auth()->guard()->login($user);
        return redirect()->route('home');
    }

    public function login(){
        return view('auth.log-in');
    }

    public function authentication(SigInRequest $request){

        $email  = strtolower($request['email']);
        $password  = $request['password'];
    
        if(Auth::attempt(['email' => $email, 'password' => $password])){
            return redirect()->route('home');
         }
         return back()
         ->withErrors(['user' => 'Tus datos son incorrectos'])
         ->withInput(request(['email']));
     }
    
      
    public function logout(Request $request){
         Auth::logout();
         $request->session()->flush();
         return redirect()->route('home');
    }
}
