<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\Room;
use App\Models\User;
use Illuminate\Support\Str;
use DB;
use Carbon\CarbonImmutable;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        set_time_limit("1800");
        if($request->ajax())
    	{   
    		$bookings = Booking::join('room_types', 'bookings.room_type_id', '=', 'room_types.id')
                               ->join('users', 'bookings.user_id', '=', 'users.id')
                               ->join('statuses', 'bookings.status_id', '=', 'statuses.id')
                               ->select('bookings.id as booking_id', 'bookings.check_in as start', 'bookings.check_out as end', 
                                         DB::raw("CONCAT(users.firstname,' ',users.lastname,' / ', statuses.name) AS title"),
                                         DB::raw("CONCAT(users.firstname,' ',users.lastname) AS huesped"),
                                        'room_types.id as room_id','room_types.name',
                                        'statuses.name as status', 'statuses.state as color')
                                   ->whereDate('bookings.check_in', '>=', $request->start)
                                   ->whereDate('bookings.check_out',   '<=', $request->end)
                                   ->get();   

            return response()->json($bookings);
    	}

    	return view('administrator.booking.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::All();
        return view('administrator.booking.create')->with('users', $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        request()->validate([
            'firstname'  => 'required',
            'dni'        => 'required|unique:users,dni',
            'email'      => 'required|unique:users,email|regex:/^.+@.+$/i',
            'phone'      => ['required','numeric','unique:users,phone'],
            'room_id'   => 'required',
            'check_in'  => 'required|before_or_equal:check_out|different:check_out',
            'check_out' => 'required|after_or_equal:check_in|different:check_in',
        ],
        [
            'firstname.required' => 'Introduzca el nombre',
            'lastname.required'  => 'Introduzca el apellido',
            'dni.required'       => 'Introduzca el DNI',
            'dni.unique'         => 'Este DNI ya se encuentra registrado',
            'email.required'     => 'Introduzca el correo electrónico',
            'email.unique'       => 'Este email ya se encuentra registrado',
            'phone.required'     => 'Introduzca el telefono',
            'phone.unique'       => 'Este telefono ya se encuentra registrado',
            'room_id.required'   => 'Seleccione la habitacion',
            'check_in.required'          => 'Introduzca la fecha inicial de disponibilidad',
            'check_in.before_or_equal'   => 'La fecha tiene que ser menor a la fecha final',
            'check_in.different'         => 'La fecha no puede ser igual a fecha final',
            'check_out.required'            => 'Introduzca la fecha fin de disponibilidad',
            'check_out.after_or_equal'      => 'La fecha tiene que ser mayor a la fecha inicial',
            'check_out.different'           => 'La fecha no puede ser igual a fecha inicial'
        ]);


        $guest = new User($request->all());
        $guest->password = Str::random(8);
        $guest->save();
       
        $booking = new Booking($request->all());
        $booking->check_out = CarbonImmutable::parse($request->check_out.' '.'20:00:00');
        $booking->user_id = $guest->id;
        $booking->room_id = $request->room_id;
        $booking->status_id = 1;
        $booking->save();

        return redirect()->route('booking.index');
   
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //     if($request->type == 'update'){
    //         $validator = Validator::make($request->all(), [
    //             'firstname' => 'required',
    //             'lastname'  => 'required',
    //             'dni'       => 'required|numeric',
    //             'email'     => 'required|regex:/^.+@.+$/i',
    //             'phone'     => 'required|numeric',
    //             'start'     => 'required|before_or_equal:end|different:end',
    //             'end'       => 'required|after_or_equal:start|different:start',
    //         ],
    //         [
    //             'firstname.required'      => 'Introduzca el nombre del huesped',
    //             'lastname.required'       => 'Introduzca el apellido del huesped',
    //             'dni.required'            => 'Introduzca el número de identificación',
    //             'dni.numeric'             => 'Solo numeros',
    //             'email.required'          => 'Introduzca el correo electrónico del huesped',
    //             'email.regex'             => 'Intruduzca un email valido',
    //             'phone.required'          => 'Intruduzca el numero telefonico',
    //             'phone.numeric'           => 'Solo numeros',
    //             'start.required'          => 'Introduzca la fecha inicial de disponibilidad',
    //             'start.before_or_equal'   => 'La fecha tiene que ser menor a la fecha final',
    //             'start.different'         => 'La fecha no puede ser igual a fecha final',
    //             'end.required'            => 'Introduzca la fecha fin de disponibilidad',
    //             'end.after_or_equal'      => 'La fecha tiene que ser mayor a la fecha inicial',
    //             'end.different'           => 'La fecha no puede ser igual a fecha inicial'
    //         ]);    
            
    //         if ($validator->passes()) {

    //                 $guest = Guest::findOrFail($request->guest_id);

    //                 if($guest->update($request->all())){
                        
    //                     $reserva = Reservation::findOrFail($id);
                    
    //                     if($reserva->update($request->all())){
    //                         return Response()->json(["success" => true,"data"=>$reserva]);
    //                     }
    //                 }             
    //         }

    //         return response()->json(['error'=>$validator->errors()]);
    //     }
    //     if($request->type == 'updateDate'){
    //         Reservation::findOrFail($id)->update(['start' => $request->start, 'end' => $request->end]);
    //         return Response()->json(["success" => true]);
            
    //     }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function confirm(Request $request){
        dd($request->booking);
        Booking::where('id',$request->booking)->update(['status_id' => 2]);
        return Response()->json(["success" => true]);
    }

    public function finish(Request $request){
        Booking::where('id',$request->booking)->update(['status_id' => 3]);
        return Response()->json(["success" => true]);
    }
}
