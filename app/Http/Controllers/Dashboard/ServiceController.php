<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HotelService;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {set_time_limit("1800");
        $hotelServices = HotelService::orderBy('id', 'desc')->get();
        return view('administrator.service.index')->with('hotelServices', $hotelServices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrator.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|unique:hotel_services',
            'file' => 'required|mimes:pdf',
        ],
        [
            'name.required' => 'Introduzca el nombre del documento',
            'name.unique'   => 'Este nombre del documento ya se encuentra registrado',
            'file.required' => 'Adjunte un archivo',
            'file.mimes'    => 'Formato de archivo invalido solo archivo PDF',
        ]);

        $document = new HotelService;
        $document->name = $request->name;

        if($request->hasFile("file")){
            $file = $request->file('file');
            $path = public_path().'/files';
            $fileName = uniqid().$file->getClientOriginalName();
            $file->move($path, $fileName);
        }
        $document->file = $fileName;
        $document->save();

        return redirect()->route('service.index')->with('success', 'Documento guardado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $file = HotelService::findOrFail($id);
        return view('administrator.service.edit')->with('file', $file);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name' => 'required|unique:hotel_services,name,'. $id,
            'file' => 'required|mimes:pdf',
        ],
        [
            'name.required' => 'Introduzca el nombre del documento',
            'name.unique'   => 'Este nombre del documento ya se encuentra registrado',
            'file.required' => 'Adjunte un archivo',
            'file.mimes'    => 'Formato de archivo invalido solo archivo PDF',
        ]);

       $file = HotelService::findOrFail($id);

       $filePath = public_path().'/files/'.$file->file;

//       dd($filePath);

       if(file_exists($filePath) && !empty($file->file)){
            unlink($filePath);
       }

       if($request->hasFile("file")){
        $file = $request->file('file');
        $path = public_path().'/files';
        $fileName = uniqid().$file->getClientOriginalName();
        $file->move($path, $fileName);   
        }

        HotelService::where('id', $id)->update(['name' => $request->name, 'file' => $fileName]);
        
        return redirect()->route('service.index')->with('success', 'Documento actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = HotelService::findOrFail($id);

       $filePath = public_path().'/files/'.$file->file;


       if(file_exists($filePath) && !empty($file->file)){
            unlink($filePath);
       }

       $file->delete();
        
        return redirect()->route('service.index')->with('success', 'Documento eliminado con éxito');
    }
}
