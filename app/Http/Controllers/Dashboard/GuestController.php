<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {set_time_limit("1800");
        $guests = User::where('admin', false)->get();
        return view('administrator.user.guests.index')->with('guests', $guests);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrator.user.guests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'firstname'  => 'required',
            'lastname'   => 'required',
            'dni'        => 'required|unique:users,dni',
            'email'      => 'required|unique:users,email|regex:/^.+@.+$/i',
            'phone'      => ['required','numeric','unique:users,phone'],
            'password'   => 'required|min:8',
        ],
        [
            'firstname.required' => 'Introduzca el nombre',
            'lastname.required'  => 'Introduzca el apellido',
            'dni.required'       => 'Introduzca el DNI',
            'dni.unique'         => 'Este DNI ya se encuentra registrado',
            'email.required'     => 'Introduzca el correo electrónico',
            'email.unique'       => 'Este email ya se encuentra registrado',
            'phone.required'     => 'Introduzca el telefono',
            'phone.unique'       => 'Este telefono ya se encuentra registrado',
            
        ]);

        $guest = new User($request->all());
        $guest->save();

        return redirect()->route('guest.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guest = User::findOrFail($id);
        return view('administrator.user.guests.edit')->with('guest', $guest);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'firstname' => 'required',
            'lastname'  => 'required',
            'dni'       => 'required|unique:users,dni,'. $id,
            'email'     => 'required|unique:users,email,'. $id.'|email|regex:/^.+@.+$/',
            'phone'     => 'required|unique:users,phone,'. $id,
        ],
        [
            'firstname.required' => 'Introduzca el nombre',
            'lastname.required'  => 'Introduzca el apellido',
            'dni.required'       => 'Introduzca el DNI',
            'dni.unique'         => 'Este DNI ya se encuentra registrado',
            'email.required'     => 'Introduzca el correo electrónico',
            'email.unique'       => 'Este email ya se encuentra registrado',
            'phone.required'     => 'Introduzca el telefono',
            'phone.unique'       => 'Este telefono ya se encuentra registrado',
        ]);

        User::findOrFail($id)->update(['firstname' => $request->firstname,'lastname' => $request->lastname,
                                       'dni' => $request->dni,'email' => $request->email,'phone' => $request->phone]);
        
        return redirect()->route('guest.index')->with('success', 'Huesped actualizado sastifactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd("En desarrollo");
    }
}
