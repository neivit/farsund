<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Restriction;
use App\Models\RoomType;

class RestrictionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {set_time_limit("1800");
        $restrictions = Restriction::orderBY('id', 'desc')->get();
        return view('administrator.restriction.index')
        ->with('restrictions', $restrictions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roomType = RoomType::orderBy('id', 'desc')->get();
        return view('administrator.restriction.create')->with('roomType', $roomType);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $restriction = new Restriction($request->all());
        $restriction->save();

        return redirect()->route('restriction.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $restriction = Restriction::find($id);
        return view('administrator.restriction.edit')->with('restriction',$restriction);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name'   => 'required|unique:restrictions,name,'. $id,
            'start'  => 'required|before_or_equal:end|different:end',
            'end'    => 'required|after_or_equal:start|different:start',
            'min_book' => 'required|numeric'
        ],
        [
            'name.required'    => 'Introduzca el nombre de la restricción',
            'name.unique'      => 'Este nombre ya se encuentra registrado',
            'start.required'   => 'Introduzca la fecha inicial',
            'start.before_or_equal' => 'La fecha inicial tiene que ser menor a la fecha fin',
            'start.different'       => 'La fecha inicial debe ser diferente a la fecha fin',
            'end.required'          => 'Introduzca la fecha fin',
            'end.after_or_equal'    => 'La fecha fin tiene que ser menor a la fecha inicial',
            'end.different'         => 'La fecha fin debe ser diferente a la fecha inicial',
            'min_book.required'     => 'Introduzca el numero minimo de reserva',
            'min_book.numeric'      => 'Este campo es numerico',
        ]);

        Restriction::findOrFail($id)->update(['name' => $request->name, 'start' => $request->start, 'end' => $request->end, 'min_book' => $request->min_book]);
        
        return redirect()->route('restriction.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restriction = Restriction::find($id)->delete();
        return redirect()->route('restriction.index');
    }
}
