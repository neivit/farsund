<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RoomType;
use App\Models\Extra;
use Illuminate\Validation\Rule;
use DB;

class RoomTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {set_time_limit("1800");
        $roomTypes = RoomType::orderBy('name')->get();
        return view('administrator.room_type.index')
        ->with('roomTypes',$roomTypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $extras = Extra::orderBy('name')->get();
        return view('administrator.room_type.create')->with('extras',$extras);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name'         => 'required|unique:room_types,name',
            'description'  => 'required',
            'adult'        => 'required',
            'children'     => 'required',
            'price'        => 'required|numeric',
            'refundable'   => Rule::in(['1','0']),
        ],
        [
            'name.required'        => 'Introduzca el tipo de habitación',
            'name.unique'          => 'Este tipo de habitación ya se encuentra registrado',
            'description.required' => 'Introduzca la descripción de la habitación',
            'adult.required'       => 'Seleccione la capacidad para adultos',
            'children.required'    => 'Seleccione la capacidad para ninos',
            'price.required'       => 'Introduzca el precio',
            'price.numeric'        => 'Este campo solo es numerico',
            'refundable.in'        => 'Seleccione si es reembolsable o no',
        ]);

        $roomType = new RoomType($request->all());
        $roomType->save();

        if($request->extra_id > 0){
            foreach($request->extra_id as $extras){
                DB::table("extra_roomtype")->insert([
                    'roomtype_id' => $roomType->id, 
                    'extra_id'     => $extras,
                ]);
                //$extras->roomTypes()->attach($roomType->id, ['optional' => $request->optional]);
            }
        }
        

        return redirect()->route('roomtype.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roomType = RoomType::find($id);
        $extras = DB::table('extras')
                    ->join('extra_roomtype', 'extras.id', '=', 'extra_roomtype.extra_id')
                    ->select('extras.*', 'extra_roomtype.extra_id as extra_id')
                    ->where('roomtype_id', '=', $roomType->id)->get();
        return view('administrator.room_type.edit')
        ->with('roomType',$roomType)
        ->with('extras',$extras);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name'         => 'required|unique:room_types,name,'. $id,
            'description'  => 'required',
            'adult'        => 'required',
            'children'     => 'required',
            'price'        => 'required|numeric',
        ],
        [
            'name.required' => 'Introduzca el tipo de habitación',
            'name.unique'   => 'Este tipo de habitación ya se encuentra registrado',
        ]);

        RoomType::findOrFail($id)->update(['name' => $request->name,
                                           'description' => $request->description,
                                           'adult' => $request->adult,
                                           'children' => $request->children,
                                           'price' => $request->price
                                         ]);
        
        if($request->extra_id > 0){
         foreach($request->extra_id as $extras){
            DB::table('extra_roomtype')->where('booking_id', $id)->where('extra_id', $extras)->update(['name' => $extras]);
         }
        }
        
        return redirect()->route('roomtype.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
