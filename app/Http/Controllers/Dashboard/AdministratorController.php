<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class AdministratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {set_time_limit("1800");
        $administrators = User::where('admin', true)->where('id', '<>', auth()->id())->get();
        return view('administrator.user.administrators.index')->with('administrators', $administrators);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrator.user.administrators.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'firstname'  => 'required',
            'lastname'   => 'required',
            'dni'        => 'required|unique:users,dni',
            'phone'      => 'required|numeric|unique:users,phone',
            'email'      => 'required|regex:/(.*)@hotelfarsund\.es$/i|unique:users,email',
            'password'   => 'required|min:8|',
        ],
        [
            'firstname.required'    => 'Introduzca el nombre',
            'lastname.required'     => 'Introduzca el apellido',
            'dni.required'      => 'Introduzca el DNI',
            'dni.unique'        => 'Este DNI ya se encuentra registrado',
            'phone.required'    => 'Introduzca el Telefono',
            'phone.unique'      => 'Este Telefono ya se encuentra registrado',
            'phone.numeric'      => 'Este campo es numerico',
            'email.required'    => 'Introduzca el correo electrónico',
            'email.regex'       => 'Introduzca un correo electrónico valido ejemplo xxx@hotelfarsund.es',
            'email.unique'      => 'Este email ya se encuentra registrado',
            'password.required' => 'Introduzca la contraseña',
        ]);

        $administrator = new User($request->all());
        $administrator->admin = true;
        $administrator->save();
        return redirect()->route('administrator.index')->with('success', 'Usuario registrado sastifactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $administrator = User::findOrFail($id);
        return view('administrator.user.administrators.edit')->with('administrator', $administrator);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        request()->validate([
            'firstname'  => 'required',
            'lastname'   => 'required',
            'dni'        => 'required|unique:users,dni,'. $id,
            'phone'      => 'required|numeric|unique:users,phone,'. $id,
            'email'      => 'required|regex:/(.*)@hotelfarsund\.es$/i|unique:users,email,'. $id,
        ],
        [
            'firstname.required'    => 'Introduzca el nombre',
            'lastname.required'     => 'Introduzca el apellido',
            'dni.required'      => 'Introduzca el DNI',
            'dni.unique'        => 'Este DNI ya se encuentra registrado',
            'phone.required'    => 'Introduzca el Telefono',
            'phone.unique'      => 'Este Telefono ya se encuentra registrado',
            'phone.numeric'     => 'Este campo es numerico',
            'email.required'    => 'Introduzca el correo electrónico',
            'email.regex'       => 'Introduzca un correo electrónico valido ejemplo xxx@hotelfarsund.es',
            'email.unique'      => 'Este email ya se encuentra registrado',
        ]);

        User::findOrFail($id)->update(['email' => $request->email,
                                       'firstname' => $request->firstname,
                                       'lastname' => $request->lastname,
                                       'dni' => $request->dni,
                                       'phone' => $request->phone
                                      ]);
        
        return redirect()->route('administrator.index')->with('success', 'Usuario actualizado sastifactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $extra = User::find($id)->delete();
        return redirect()->route('administrator.index');
    }
}
