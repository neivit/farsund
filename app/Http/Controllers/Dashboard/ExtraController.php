<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Extra;
use App\Models\RoomType;

class ExtraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {set_time_limit("1800");
        $extras = Extra::orderBy('name')->get();
        $roomTypes = RoomType::all();
        return view('administrator.extras.index')
        ->with('extras',$extras)
        ->with('roomTypes',$roomTypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrator.extras.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $extra = new Extra($request->all());
        $extra->save();

        return redirect()->route('extra.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $extra = Extra::find($id);
        return view('administrator.extras.edit')->with('extra',$extra);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name'   => 'required|unique:extras,name,'. $id,
            'price'  => 'required|numeric'
        ],
        [
            'name.required'  => 'Introduzca el extra',
            'name.unique'    => 'Este extra ya se encuentra registrado',
            'price.required' => 'Introduzca el precio',
            'price.numeric'  => 'Este campo es nuemrico',
        ]);

        Extra::findOrFail($id)->update(['name' => $request->name, 'price' => $request->price]);
        
        return redirect()->route('extra.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $extra = Extra::find($id)->delete();
        return redirect()->route('extra.index');
    }
}
