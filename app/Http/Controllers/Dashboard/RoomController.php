<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RoomType;
use App\Models\Room;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {set_time_limit("1800");
        $rooms = Room::orderBy('id', 'asc')->get();
        return view('administrator.room.index')
        ->with('rooms',$rooms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roomTypes = RoomType::orderBy('name')->get();
        return view('administrator.room.create')->with('roomTypes',$roomTypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'room_type_id'   => 'required|not_in:0',
            'room_number'    => 'required|numeric',
        ],
        [
            'room_type_id.required'   => 'Seleccione la tipo habitación',
            'room_type_id.not_in'     => 'Seleccione la tipo habitación',
            'room_number.required'    => 'Introduzca el numero de habitación',
            'room_number.numeric'     => 'Este campo solo es numerico',
        ]);

        $room = new Room($request->all());
        $room->status_id = 4;
        $room->save();

        return redirect()->route('room.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd("En desarrollo");
    }
}
