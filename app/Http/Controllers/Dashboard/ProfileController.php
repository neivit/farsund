<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {set_time_limit("1800");
        set_time_limit("1800");
        return view('administrator.profile.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function editProfile(){
        return view('administrator.profile.edit');
    }

    public function updateProfile(Request $request){
        request()->validate([
            'firstname'  => 'required',
            'lastname'   => 'required',
            'dni'        => 'required|unique:users,dni,'. auth()->user()->id,
            'phone'      => 'required|numeric|unique:users,phone,'. auth()->user()->id,
            'email'      => 'required|regex:/(.*)@hotelfarsund\.es$/i|unique:users,email,'. auth()->user()->id,
        ],
        [
            'firstname.required'    => 'Introduzca el nombre',
            'lastname.required'     => 'Introduzca el apellido',
            'dni.required'      => 'Introduzca el DNI',
            'dni.unique'        => 'Este DNI ya se encuentra registrado',
            'phone.required'    => 'Introduzca el Telefono',
            'phone.unique'      => 'Este Telefono ya se encuentra registrado',
            'phone.numeric'     => 'Este campo es numerico',
            'email.required'    => 'Introduzca el correo electrónico',
            'email.regex'       => 'Introduzca un correo electrónico valido ejemplo xxx@hotelfarsund.es',
            'email.unique'      => 'Este email ya se encuentra registrado',
        ]);

        User::findOrFail(auth()->user()->id)->update(['email' => $request->email,
                                       'firstname' => $request->firstname,
                                       'lastname' => $request->lastname,
                                       'dni' => $request->dni,
                                       'phone' => $request->phone
                                      ]);
        
        return redirect()->route('profile.index')->with('success', 'Datos actualizado sastifactoriamente');
    }

    public function editPass(){
        return view('administrator.profile.my-password');
    }

    public function updatePass(Request $request){
        $user = User::find(auth()->user()->id)->update(['password' => $request->password]);

        return redirect()->route('profile.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
