<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\Order;
use App\Http\Requests\BookingRequest;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Support\Facades\Log;
use App\Redsys\RedsysAPI;

class BookingPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookingRequest $request, $room)
    {
        
        $check_in = Carbon::parse($request->check_in);
        $check_out = Carbon::parse($request->check_out);
        
        $bookMin = date_diff($check_in, $check_out)->format('%a');


        $disponibilidad = Booking::validateBook($request->check_in, $request->check_out, $room);

        $restriction = Booking::restriction($room, $request->check_in, $request->check_out);

        if($disponibilidad){
            return redirect()->back()->with('status', 'Esta habitacion ya se encuentra ocupada en esa fechas');
        }
        if($bookMin < $restriction ){
            return redirect()->back()->with('status', 'La reserva minima para esta temporada es de'. $restriction);
        }
        else{
            $booking = new Booking($request->all());
            $booking->check_out = CarbonImmutable::parse($request->check_out.' '.'20:00:00');
            $booking->user_id = auth()->user()->id;
            $booking->room_type_id = $room;
            $booking->status_id = 1;
            $booking->save();

            Order::create($request, $booking->id);

            $redsys = new RedsysAPI;

            $redsys->setParameter("DS_MERCHANT_AMOUNT", 100 * 100);
            $redsys->setParameter("DS_MERCHANT_ORDER", rand(0,9999));
            $redsys->setParameter("DS_MERCHANT_MERCHANTCODE", '120007158');
            $redsys->setParameter("DS_MERCHANT_CURRENCY", '978');
            $redsys->setParameter("DS_MERCHANT_TRANSACTIONTYPE", '0');
            $redsys->setParameter("DS_MERCHANT_TERMINAL", '1');
            $redsys->setParameter("DS_MERCHANT_MERCHANTURL", env('APP_URL'));
            $redsys->setParameter("DS_MERCHANT_URLOK", route('confirm'));
            $redsys->setParameter("DS_MERCHANT_URLKO", route('confirm'));
    
            $params = $redsys->createMerchantParameters();
    
            $claveSHA256 = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7';
            $firma = $redsys->createMerchantSignature($claveSHA256);
    
    
            return view('payment.redsys', [
                'params' => $params,
                'firma'  => $firma,
                'versionFirma' => 'HMAC_SHA256_V1',
                'url' => 'https://sis-t.redsys.es:25443/sis/realizarPago'
            ]);

        }

        /*Craer evento*/
        
        // $data = array(
        //     'room'       => $booking->room->roomType->name,
        //     'dayStart'   => $booking->check_in->isoFormat('dddd'),
        //     'dateStart'  => $booking->check_in->toFormattedDateString(),
        //     //'hourStart'  => Carbon::parse($room->check_in)->format('g:i a'),
        //     'dayEnd'     => $booking->check_out->isoFormat('dddd'),
        //     'dateEnd'    => $booking->check_out->toFormattedDateString(),
        //     //'hourEnd'    => Carbon::parse($room->check_out)->format('g:i a'),
        //     'price'      => $booking->room->price
        // );

        // Mail::send('mail.guest-booking', $data, function($message) use ($data) {
        // $message->to(auth()->user()->email)
        // ->subject('Reserva');
        // $message->from("info@hotelfarsund.es", "Hotel Farsund");
        // });

        /*Craer evento*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return redirect()->route('rooms')->with('status', 'Su reserva se ha registrado satisfactoriamente!');
        // $redsys = new RedsysAPI;

        // $version = $_GET["Ds_SignatureVersion"]; 
        // $params = $_GET["Ds_MerchantParameters"]; 
        // $signatureRecibida = $_GET["Ds_Signature"]; 

        // $decodec = $redsys->decodeMerchantParameters($params);

        // $codigoRespuesta = $redsys->getParameters("Ds_Response");

        // $claveModuloAdmin = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7'; 
        // $signatureCalculada = $redsys->createMerchantSignatureNotif($claveModuloAdmin, $params); 

        // if ($signatureCalculada === $signatureRecibida) { 
        //     return redirect()->back()->with('status', 'Su reserva se ha registrado satisfactoriamente!');
        // } else { 
        //     return redirect()->back()->with('status', 'El proceso de pago para la reserva ha sido cancelada'); 
        // }

                
         
            
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function approbal(Request $request){
        $parameters = Redsys::getMerchantParameters($request->input('Ds_MerchantParameters'));
        $DsResponse = $parameters["Ds_Response"];
        $DsResponse += 0;
  
         if ($DsResponse <= 99) { 
              return redirect()->back()->with('status', 'Su reserva se ha registrado satisfactoriamente!');
         } 
         else if($DsResponse == 9915) {
          return redirect()->back()->with('status', 'El proceso de pago para la reserva ha sido cancelada');
         }
    }
}
