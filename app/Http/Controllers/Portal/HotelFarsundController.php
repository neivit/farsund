<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RoomType;
use App\Models\Extra;
use App\Models\HotelService;
use Carbon\Carbon;
use Mail;

class HotelFarsundController extends Controller
{
    public function home(){
        set_time_limit("1800");
        return view('index');
    }

    public function about(){
        return view('about');
    }

    public function rooms(Request $request)
    {
        set_time_limit("1800");
        $checkIn = Carbon::parse($request->check_in)->format('Y-m-d');
        $checkOut = Carbon::parse($request->check_out)->format('Y-m-d'); //->format('Y-m-d H:i:s')
        $adult = $request->adult;
        $children = $request->children;

        $rooms = RoomType::OrderBy('name')->get();

        $extras = Extra::get('name');
        
        return view('room')
             ->with('checkIn', $checkIn)
             ->with('checkOut', $checkOut)
             ->with('adult', $adult)
             ->with('children', $children)
             ->with('rooms', $rooms)
             ->with('extras', $extras);
    }

    public function book($id, $chechin = null, $checkout = null)
    {set_time_limit("1800");
        $room = RoomType::findOrFail($id);

        $total =  100;

        return view('booking')
             ->with('chechin', $chechin)
             ->with('checkout', $checkout)
             ->with('room', $room)
             ->with('total', $total);
    }

    public function services(){
        $hotelServices = HotelService::all();
        return view('service')->with('hotelServices', $hotelServices);
    }

    public function placeInterest(){
        return view('places_interest');
    }

    public function contact(){
        return view('contact');
    }

    public function send(Request $request){
        request()->validate([
            'name'   => 'required',
            'email'  => 'required',
            'message'   => 'required',
            'check1' => 'required',
            'check2' => 'required',
        ]);

        $data = array(
            'name'  => $request->name,
            'email' => $request->email,
            'msn'  => $request->message
        );
        Mail::send('mail.contact', $data, function ($message) use ($data) {
            //$message->to($data['email'])
            $message->to("info@hotelfarsund.es")
                ->subject('Contacto');
            $message->from("info@hotelfarsund.es", "Hotel Farsund");
        });

        return redirect()->back()->with('success', 'Mensaje enviado sastifactoriamente');
    }

    public function policyPrivacy()
    {
        return view('policy-privacy.index');
    }

    public function termsConditions()
    {
        return view('policy-privacy.terms-conditions');
    }
}
