<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [            
            'check_in'  => 'required|before_or_equal:check_out|different:check_out',
            'check_out' => 'required|after_or_equal:check_in|different:check_in',
            'firstname' => 'required',
            'dni'       => 'required|numeric',
            'email'     => 'required|regex:/^.+@.+$/i',
            'phone'     => 'required|numeric',
        ];
    }
}
