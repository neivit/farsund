<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class SigUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'  => 'required',
            'lastname'   => 'required',
            'dni'        => 'required|unique:users,dni',
            'email'      => 'required|regex:/^.+@.+$/i|unique:users,email',
            'phone'      => 'required|unique:users,phone',
            'password'   => 'required|min:8|',
        ];
    }
}
