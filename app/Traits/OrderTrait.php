<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\Models\Order;
use Carbon\Carbon;

trait OrderTrait{

    /*
    // Function para guardar order
    */
    public static function create(Request $request, $booking){
        $order = new Order();
        $order->order_number = $booking;
        $order->date = Carbon::now();
        $order->price = $request->price;
        $order->iva = $request->price/1.21 + $request->price;
        $order->total = $request->price * 1.21;
        $order->booking_id = $booking;
        $order->save();
    }


}