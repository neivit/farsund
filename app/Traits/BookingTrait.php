<?php

namespace App\Traits;

use App\Models\Booking;
use App\Models\Restriction;
use App\Models\RoomType;

trait BookingTrait{

    /*
    * Metodo para consultar 
    * disponibilidad
    * de reserva por habitacion
    */
    public static function validateBook($start, $end, $room){
        
        $booking = booking::whereBetween('check_out', [$start, $end])->orWhereBetween('check_in', [$start, $end])->where('room_type_id', $room)->exists();

        return $booking;
    }


    /*
    * Metodo para consultar 
    * el min de reserva
    * por temporada
    */
    public static function restriction($room, $start, $end){
        
        $room = RoomType::find($room);        
        $restrinction = Restriction::where('room_type_id', $room->id)
                                   ->where('start', '<=', $start)
                                   ->where('end', '>=', $end)
                                   ->first();
        
        if(!empty($restrinction)){
            return $restrinction->min_book;
        }
        else{
            return false;
        }
 
    }


   


    


}